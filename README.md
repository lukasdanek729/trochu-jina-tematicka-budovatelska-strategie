# Global Warming 

Hra Global Warming vznikala jako semestrální práce pro předmět Programování a Algoritmizace 2 (PA2) na FIT ČVUT. Hra se liší od běžných budovatelských strategií "zasazením", kdy hráč buduje LQBT vesničku a musí odolat různým nástrahám, jak náhodně generovaným událostem tak i ovládání, které nepatří k těm nejúžasnějším, to musím přiznat. Při vytváření hry jsem se nesnažil nikoho urazit, jen jsem chtěl aby byla trochu šílená a vyčnívala oproti ostatním hrám/semestrálním pracím.

****

Hra vznikala, když jsem s jazykem C++ začínal, tudíž bych teď dělal pár věcí jinak, či efektivněji (například změna ncurses za jinou grafickou knihovnu...). 

Pro úspěšnou kompilaci je třeba, aby měl uživatel nainstalované `ncurses` v terminálu a pro vygenerování dokumentace `doxygen`. 

Pokud terminál podporuje rozšíření barev, je hra o dost barvitější a "PRIDE-flag colour palette" věrohodnější. 

Hra má nastavené hratší prodlevy mezi generováním událostí a také rychlejší generování surovin, aby za krátký čas mohlo být demonstrováno vše, co hra umí a dokáže.

****

**Hra dokáže:**

1. ze souboru nahraje definici světa (mapu, vzhled objektů na mapě, zdroje, lokace protihráčů, ...)
2. ze souboru nahraje upravitelné parametry možných budov (vstupy, výstupy, cena stavby, ...)
3. ze souboru nahraje definici akcí (katastrofy, globální změny, ...)
4. umožní simulovat běh světa - přidávání budov, těžba, ...
5. implementuje jednoduchý hodnotící systém (počet QUEERS, zdrojů ...)
6. umožňuje ukládat a načítat rozehrané hry

Engine funguje jako real-time a využívá uživatelské rozhraní s terminálovou grafikou s použitím ncurses knihovny.

Hra využívá polymorfizmu u typu, parametru budov a různým náhodných událostí. 

****

**Popis hry:**

Hlavní náplní hry "Global Warming" je co nejvíce zateplit planetu, neboli aby měli Queer (LGBTQ) obyvatelé co největší zastoupení vzhledem k celkové populaci. Důležité také je, aby veškerá Queer populace v důsledku akcí hráče, nebo i prostředí, nevymřela, to by se spolku Tradiční rodiny líbilo... 

*Hra obsahuje dané budovy:*

 - GYM  - generuje "Money" a "Joy"  

 - Adoption Centre - generuje "Money" a "Joy" 

 - StarBucks  - generuje "IceCoffee" a "Cupcakes" 

 - Residence  - čím více rezidencí, či větší úroveň, tím větší kapacita Queers, také nějakou malou část generuje

 - Painting Shop - aby mohla být všude duha a veselo, je třeba obchod s barvami

 - P-Flag Centre - pomáhá lidem vyrovnat se s orientací, zvětšuje počet Queer populace

Každou budovu je možné vylepšit o několik úrovní, aby generovali více prostředků nebo jich byly možné více skladovat.
Když se budovy staví, vylepšují, negenerují nic.



*Hra obsahuje dané zdroje, které je nutné generovat:*

 - Money  - jsou potřeba pro stavbu a vylepšení budov
 - IceCoffee - aby se na pracovišti nespalo, je nutná pro dodání energie, pro stavbu, či vylepšení budov
 - Cupcakes - aby populace nevyhladověla, je třeba ji něčím krmit
 - Joy  - do práce se musí i radostí, proto je jedním z důležitých parametrů při stavbě, také se postupně spotřebovává
			   když se generuje víc a víc Queers
 - Colors - aby budovy nějak vypadaly, je třeba jim dát nějakou pěknout fasádu, nutná surovina pro stavbu a vylepšení
 - Queers - celkový počet teplé populace

*Hra obsahuje náhodné události:*

 - Traditinal Family Raid - ubere určité procentro "Joy" a "Queers"
 - Rainy Day  - ubere určité procento "Bio Cupcakes" a "Joy"
 - Crusade - ubere část "Money"
 - PRIDE Festival - přidá 10% všech surovin a Queers
(názvy se mohou ještě do finální verze změnit, popřípadě nějaké další události se mohou přidat)

****

**Ovládání hry:**

Hráč si v menu vybere: 

 1. Načtení uložené hry z jednoho ze šesti slotů
 2. Spuštění nové hry
    - zadá velikost mapy, která musí splňovat min. velikost a musí se vejít do terminálového okna
    - vybere si dostupnou obtížnost (od ní se bude odvíjet počet počátečních surovin, generování akcí)
 3. Ukončení celé aplikace

Hra bude vykreslovat průběh do terminálu ( mapu pro stavbu budov ve formátu sítě z čtvercových parcel, zdroje pro stavbu, celkový počet Queers)

Hráči se po stisknutí "ENTER" klávesy zobrazí možnosti, kterými může ovlivnit dění světa:
 - save     - uloží do herního slotu, jehož jméno hráč na začátku hry zadal

 - quit     - uloží hru a odejde do hlavního menu

 - build    - postaví budovu na zadané souřadnice na mapě, které hráč poté zadá ve formátu <x>[SPACE]<y>

 - upgrade  - vylepší budovu na zadaných souřadnicích na mapě, které hráč zadá

 - destroy  - zničí budovu na zadaných souřadnicích na mapě a vrátí hráči 40% surovin z nákladů budovy


****

**Snímky ze hry:**

Menu hry a nově založená hra:

<img src="menu.PNG" alt="drawing" width="500" />

<img src="new_game.PNG" alt="drawing" width="500" />



Načtená uložená pozice a zpráva o úpravě surovin proběhlou událostí:

<img src="game_in_progress.PNG" alt="drawing" width="500" />

<img src="info_message.PNG" alt="drawing" width="500" />



ASCII-ART události PRIDE a Deštivého dne:

<img src="event-pride.PNG" alt="drawing" width="500" />

<img src="event-rainy_day.PNG" alt="drawing" width="500" />
