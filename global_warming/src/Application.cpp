#include "Application.hpp"

Application::Application()
        : m_Quit(false), m_FileReader(), m_Interface() {
    m_Interface.Initialize();
}

void Application::SetGame() {
    while (true) {
        E_MenuChoice choice = m_Interface.ChooseInMainMenu();
        ///checks if user want to star new game
        if (choice == E_MenuChoice::NEW_GAME) {
            m_FileReader.SetSlot(m_Interface.ChooseGameSlot());
            m_Game = std::make_unique<Game>(m_FileReader, m_Interface.ChooseDifficulty(), m_Interface.ChooseMapSize());
            if (!m_Game->IsInitialized()) {
                m_Interface.ShowError("Game cannot be initialized!");
                m_Game.reset();
                m_Quit = true;
            } else
                m_Game->SetInterface(m_Interface);
            break;
        }
            ///check if user wants to load played game and checks if terminal is big enough
        else if (choice == E_MenuChoice::LOAD_GAME) {
            m_FileReader.SetSlot(m_Interface.ChooseGameSlot());
            m_Game = std::make_unique<Game>(m_FileReader);
            if (!m_Game->IsInitialized() || !m_Game->CanShowWholeMap(m_Interface)) {
                m_Interface.ShowError("Game cannot be loaded!");
                m_Game.reset();
                continue;
            } else {
                m_Game->SetInterface(m_Interface);
                break;
            }
        }
            ///checks if user wants to quit menu and close application
        else if (choice == E_MenuChoice::QUIT_GAME) {
            m_Quit = true;
            break;
        }
    }
}

void Application::Run() {
    ///Initializes game ofter another until quit is pressed
    while (true) {
        /// Loop of one initialized game
        SetGame();
        if (m_Quit)
            break;
        while (m_Game->IsRunning()) {
            ///catches if user want to interact with game
            if (m_Interface.EnterPressed()) {
                m_Game->ProcessInput(m_Interface);
                if (!m_Game->IsRunning())
                    break;
                m_Interface.RefreshWindows();
                m_Game->Update(m_Interface);
            } else
                m_Game->Update(m_Interface);
            ///game state is updated every second
            sleep(1);
        }
        m_Game.reset();
    }
}