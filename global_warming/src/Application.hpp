#pragma once

#include <unistd.h>
#include <memory>

#include "Game.hpp"
#include "Interface.hpp"
#include "E_MenuChoice.hpp"

/**
 * Application class connects game logic with visual representation, allows user to set new game or load old ones
 */
class Application {
private:

    bool                  m_Quit;
    std::unique_ptr<Game> m_Game;
    FileReader            m_FileReader;
    Interface             m_Interface;

public:
    /**
     * Constructor of Application where the first game is initialized
     */
    Application();

    /**
     * Default destructor of Application
     */
    ~Application() = default;

    /**
     * Application cannot be copied
     */
    Application(const Application &src) = delete;

    Application &operator=(const Application &src) = delete;

    /**
     * Application cannot be moved
     */
    Application(Application &&src) = delete;

    Application &operator=(Application &&src) = delete;

    /**
     * Allows player to decide if he wants play new game, load one or quit whole Application
     */
    void SetGame();

    /**
     * Allows player to play and set game and change saved games until it gets boring
     */
    void Run();
};