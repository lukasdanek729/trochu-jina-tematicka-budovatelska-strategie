#include "AdoptionCentre.hpp"

AdoptionCentre::AdoptionCentre(FileReader & reader)
    : Building() {
    /// sets character for further saving and initialize values and icon from a file
    m_SaveCode = 'A';
    reader.OpenFile("src/Binaries/AdoptionCentre.bin");
    reader.ReadIcone(m_Icon);
    ReadCommonParameters(reader);
    reader.Read(m_Cost_Money, sizeof(m_Cost_Money));
    reader.Read(m_Cost_Ice_Coffee, sizeof(m_Cost_Ice_Coffee));
    reader.Read(m_Cost_Bio_Cupcakes, sizeof(m_Cost_Bio_Cupcakes));
    reader.Read(m_Cost_Colors, sizeof(m_Cost_Colors));
    reader.Read(m_Incr_Money, sizeof(m_Incr_Money));
    reader.Read(m_Incr_Joy, sizeof(m_Incr_Joy));
    reader.Read(m_Limit_Money, sizeof(m_Limit_Money));
    m_Limit_Sum_Money = m_Limit_Money;

}

void AdoptionCentre::MultiplyAll () {
    Multiply(m_Incr_Money, m_Multipliers.m_Increment);
    Multiply(m_Incr_Joy, m_Multipliers.m_Increment);
    Multiply(m_Limit_Money, m_Multipliers.m_Limit);
    m_Limit_Sum_Money += m_Limit_Money;
    MultiplyCostsAndTime();
}

void AdoptionCentre::MultiplyCostsAndTime () {
    Multiply(m_Cost_Money, m_Multipliers.m_Cost);
    Multiply(m_Cost_Ice_Coffee, m_Multipliers.m_Cost);
    Multiply(m_Cost_Bio_Cupcakes, m_Multipliers.m_Cost);
    Multiply(m_Cost_Colors, m_Multipliers.m_Cost);

    Multiply(m_TimeForBuild, m_BuildMultiplier);
}

void AdoptionCentre::ShowCosts(Interface & interface) {
    /// creates string containing current costs and passes it to interface
    std::string costs = "You need ";
    costs += std::to_string(m_Cost_Money);
    costs += " Money, ";
    costs += std::to_string(m_Cost_Ice_Coffee);
    costs += " Ice Coffee, ";
    costs += std::to_string(m_Cost_Bio_Cupcakes);
    costs += " CupCakes, ";
    costs += std::to_string(m_Cost_Colors);
    costs += " Colors";
    interface.ShowInfo(&costs[0]);
}

bool AdoptionCentre::Affordable (const Resources & resources) const {
    return resources.CheckValue({m_Cost_Money, m_Cost_Ice_Coffee, m_Cost_Bio_Cupcakes, m_Cost_Colors},
                                Resources::MONEY | Resources::ICE_COFFEE | Resources::CUPCAKES | Resources::COLORS );
}

void AdoptionCentre::DeductCosts(Resources &resources) {
    resources.UpdateValues({m_Cost_Money, m_Cost_Ice_Coffee, m_Cost_Bio_Cupcakes, m_Cost_Colors},
                           Resources::DEDUCT | Resources::MONEY | Resources::ICE_COFFEE | Resources::CUPCAKES | Resources::COLORS);
}

void AdoptionCentre::AddLimits(Resources & resources) {
    resources.UpdateLimit({m_Limit_Money},Resources::ADD | Resources::MONEY);
}
void AdoptionCentre::DeductIncrements(Resources & resources) {
    resources.UpdateIncrement({m_Incr_Money, m_Incr_Joy}, Resources::DEDUCT | Resources::MONEY | Resources::JOY);
}
void AdoptionCentre::AddIncrements(Resources & resources) {
    resources.UpdateIncrement({m_Incr_Money, m_Incr_Joy}, Resources::ADD | Resources::MONEY | Resources::JOY);
}

bool AdoptionCentre::Destroy(Resources & resources) {
    /// building cannot be destroyed if it's unavailable
    if (!m_Available)
        return false;
    DeductIncrements(resources);
    resources.UpdateLimit({m_Limit_Sum_Money},
                           Resources::DEDUCT | Resources::MONEY);
    /// adds to resources generated values some part of building costs
    resources.UpdateValues({Remains(m_Cost_Money), Remains(m_Cost_Ice_Coffee), Remains(m_Cost_Bio_Cupcakes), Remains(m_Cost_Colors)},
                           Resources::ADD | Resources::MONEY | Resources::ICE_COFFEE | Resources::CUPCAKES | Resources::COLORS);
    return true;
}