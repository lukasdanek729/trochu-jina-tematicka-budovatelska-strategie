#include "Building.hpp"

Building::Building()
    : m_Available(false)
    , m_Level(0) {}

void Building::ReadCommonParameters(FileReader & reader) {
    /// reads parameters in defined order from a file
    reader.Read(m_Multipliers.m_Cost, sizeof(m_Multipliers.m_Cost));
    reader.Read(m_Multipliers.m_Increment, sizeof(m_Multipliers.m_Increment));
    reader.Read(m_Multipliers.m_Limit, sizeof(m_Multipliers.m_Limit));
    reader.Read(m_BuildMultiplier, sizeof(m_BuildMultiplier));
    reader.Read(m_TimeForBuild, sizeof(m_TimeForBuild));
    m_TimeToFinish = m_TimeForBuild;
}


uint32_t Building::Remains(uint32_t & value) {
    /// because costs of building are actual for next upgrade, calculates previous costs nd from that value gets
    /// remains which are returned to game

    /// in return used simplified version of calculation above
    return (value * m_DestroyMultiplier / (10 * m_Multipliers.m_Cost));
}

void Building::Multiply(uint32_t & value, const uint8_t & multiplier ) {
    /// multiplying by value and dividing by 10 to avoid using floating point variables
    value *= multiplier;
    value /= 10;
}

bool Building::CanUpgrade() const {
    return m_Level < m_MaxLevel;
}

void Building::Update(Resources & resources) {
    if (m_Available)
        return;
    /// after time to finish has passed, building is finished and became available
    else {
        if (--m_TimeToFinish == 0) {
            Finish(resources);
        }
        return;
    }
}

bool Building::Build(Resources &resources) {
    if (m_Level != 0 || !Affordable(resources))
        return false;
    DeductCosts(resources);
    MultiplyCostsAndTime();
    return true;
}

bool Building::Upgrade(Resources & resources) {
    /// checks upgrade-ability
    if (!CanUpgrade() || !m_Available || !Affordable(resources))
        return false;
    DeductCosts(resources);
    /// makes building unavailable and deducts all it's increment values
    DeductIncrements(resources);
    m_TimeToFinish = m_TimeForBuild;
    MultiplyAll();
    m_Available = false;
    return true;
}

void Building::Finish (Resources & resources) {
    /// updates game values and increases level
    AddLimits(resources);
    AddIncrements(resources);
    ++m_Level;
    m_Available = true;
}

void Building::Load(FileReader &reader) {
    /// loads values necessary for reconstruction building from played game
    reader.Read(m_Level, sizeof(m_Level));
    reader.Read(m_TimeToFinish,sizeof(m_TimeToFinish));

    /// if building is not finished makes it unavailable
    m_Available = m_TimeToFinish == 0;
    MultiplyCostsAndTime();

    /// updates all values depending on building level
    for (int16_t i = 1; i < m_Level; ++i) {
        MultiplyAll();
    }
}

void Building::Save (FileWriter & writer) const {
    writer.WriteChar(m_SaveCode);
    writer.Write(m_Level, sizeof(m_Level));
    writer.Write(m_TimeToFinish, sizeof(m_TimeToFinish));
}
