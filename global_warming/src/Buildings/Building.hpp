#pragma once

#include <vector>
#include <iostream>
#include <memory>

#include "../Interface.hpp"
#include "../Resources.hpp"
#include "../FileReader.hpp"
#include "../FileWriter.hpp"

/**
 * Building abstract class represent what every building type can do and contains parameters in common
 */
class Building {
protected:
    /**
     * Building Multipliers for each type of parameter when building is leveled-up or built
     */
    struct Multipliers {
        uint8_t m_Cost = 0;
        uint8_t m_Increment = 0;
        uint8_t m_Limit = 0;
    };

    bool m_Available;

    char m_SaveCode;
    int16_t m_Level;
    uint8_t m_BuildMultiplier;

    uint32_t m_TimeToFinish;
    uint32_t m_TimeForBuild;

    std::string m_Icon;

    Multipliers m_Multipliers;

    /**
     * Multiplier for returning resources when building is destroyed, value in tens of percent
     */
    const uint32_t m_DestroyMultiplier = 4;
    /**
     * Default max level of each building
     */
    const int16_t m_MaxLevel = 6;

    /**
     * Constructor of Building
     */
    Building();

    /**
     * Reads common parameters from a file
     * @param reader File reader for reading correct amounts of bytes and from correct file
     */
    void ReadCommonParameters(FileReader &reader);

    /**
     * Calculates value of resource to be return when building is destroyed
     * @param value Current value of resource for leveling-up
     * @return value of resource to be return when building is destroyed
     */
    uint32_t Remains(uint32_t &value);

    /**
     * Multiplies building parameter when building is upgraded or built to make difference between building levels
     * @param value Multiplied value
     * @param multiplier Multiplier value
     */
    void Multiply(uint32_t &value, const uint8_t &multiplier);

    /**
     * Multiplies every building parameter (costs, limits, increases and building time)
     */
    virtual void MultiplyAll() = 0;

    /**
     * Multiplies building costs and building time parameters
     */
    virtual void MultiplyCostsAndTime() = 0;

public:
    /**
     * Destructor of Building
     */
    virtual ~Building() = default;

    /**
     * Getter for level of building
     * @return current level of building or 0 if building is in construction
     */
    inline int16_t GetLevel() const { return m_Available ? m_Level : 0; }

    /**
     * Gets pointer to first character in game ASCII Art icon
     * @return
     */
    inline const char *GetIconPointer() { return &m_Icon[0]; }

    /**
     * Starts construction of a building
     * @param resources Game resources necessary for construction of building
     * @return true if construction started, false otherwise
     */
    bool Build(Resources &resources);

    /**
     * Starts upgrading of a building
     * @param resources Game resources necessary for updating of building
     * @return true if upgrade started, false otherwise
     */
    bool Upgrade(Resources &resources);

    /**
     * When building has finished building in m_Level == 1, costs and \pm_TimeForBuild are increased
     * @param resources Game resources for update increment and limit values
     */
    void Finish(Resources &resources);

    /**
     * Saves necessary values of building to file for eventual loading for further playing
     * @param writer File writer for writing parameters to correct file in correct form
     */
    void Save(FileWriter &writer) const;

    /**
     * Loads every necessary parameter of building
     * @param reader File reader for reading values from file
     * @return true if loading was successful, false otherwise
     */
    void Load(FileReader &reader);

    /**
     * Updates state of building in construction or finishes constructions
     * @param resources Game resources updated if construction of building is just finished
     */
    virtual void Update(Resources &resources);

    /**
     * Checks if building's level is not maxed out and still can be upgraded
     * @return true if building's level is not maxed out and still can be upgraded, false otherwise
     */
    virtual bool CanUpgrade() const;

    /**
     * Shows costs for building or upgrading building
     * @param interface Game interface where values are shown
     */
    virtual void ShowCosts(Interface &interface) = 0;

    /**
     * Finds out if game has enough resources for building or upgrading
     * @param resources Game resources for finding out their values
     * @return true if if game has enough resources for building or upgrading, false otherwise
     */
    virtual bool Affordable(const Resources &resources) const = 0;

    /**
     * Deduct game resources necessary for building or upgrading
     * @param resources Game resources for deducting their values
     */
    virtual void DeductCosts(Resources &resources) = 0;

    /**
     * Increases certain resources limits by building's update values
     * @param resources Game resources for updating their values
     */
    virtual void AddLimits(Resources &resources) = 0;

    /**
     * Decrease certain resources increment values by building's update values
     * @param resources Game resources for updating their values
     */
    virtual void DeductIncrements(Resources &resources) = 0;

    /**
     * Increases certain resources increment values by building's update values
     * @param resources Game resources for updating their values
     */
    virtual void AddIncrements(Resources &resources) = 0;

    /**
     * Adds some part of resources which were used for construction back to game before building is destroyed
     * @param resources Game resources where some values are slightly increased
     * @return true if destroying building is not in construction and resources were added, false otherwise
     */
    virtual bool Destroy(Resources &resources) = 0;

    /**
     * Duplicates building for separate use
     * @return unique pointer to newly created copy of building
     */
    virtual std::unique_ptr<Building> Clone() const = 0;
};