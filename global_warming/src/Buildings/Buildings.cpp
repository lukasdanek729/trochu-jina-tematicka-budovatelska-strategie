#include "Buildings.hpp"

Buildings::Buildings(FileReader &reader) {
    /// inserts every type of building to map
    m_Data.insert(std::make_pair(E_BuildingChoice::ADOPT_CENTRE, std::make_unique<AdoptionCentre>(reader)));
    m_Data.insert(std::make_pair(E_BuildingChoice::GYM,          std::make_unique<Gym>           (reader)));
    m_Data.insert(std::make_pair(E_BuildingChoice::PAINT_SHOP,   std::make_unique<PaintingShop>  (reader)));
    m_Data.insert(std::make_pair(E_BuildingChoice::PFLAG_CENTRE, std::make_unique<PFlagCentre>   (reader)));
    m_Data.insert(std::make_pair(E_BuildingChoice::STAR_BUCKS,   std::make_unique<StarBucks>     (reader)));
    m_Data.insert(std::make_pair(E_BuildingChoice::RESIDENCE,    std::make_unique<Residence>     (reader)));
}