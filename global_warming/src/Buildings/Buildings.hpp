#pragma once

#include <map>
#include <memory>

#include "../E_BuildingChoice.hpp"
#include "Building.hpp"
#include "AdoptionCentre.hpp"
#include "Gym.hpp"
#include "PaintingShop.hpp"
#include "PFlagCentre.hpp"
#include "Residence.hpp"
#include "StarBucks.hpp"

/**
 * Buildings class includes every constructable building
 */
class Buildings {
private:

    std::map<E_BuildingChoice, std::unique_ptr<Building>> m_Data;
public:

    /**
     * Constructor of Buildings with File reader as parameter
     * @param reader File reader for reading correct amounts of bytes and from correct file
     */
    Buildings(FileReader &reader);

    /**
     * Destructor of Buildings
     */
    ~Buildings() = default;

    /**
     * Makes new copy of selected building
     * @param choice Building type which is being cloned
     * @return unique pointer to copied selected type of building
     */
    inline std::unique_ptr<Building> CloneBuilding(const E_BuildingChoice &choice) { return m_Data[choice]->Clone(); }

    /**
     * Check if selected type of building is affordable to construct
     * @param choice Building type whose affordability is being checked
     * @param resources Resources which are being checked if contains enough to build selected type of building
     * @return true if resources contains enough to build selected type of building, false otherwise
     */
    inline bool Affordable(const E_BuildingChoice &choice, const Resources &resources) {
        return m_Data[choice]->Affordable(resources);
    }

    /**
     * Shows costs of selected building type
     * @param choice Building type whose costs are being shown
     * @param interface Game interface where building costs are shown
     */
    inline void ShowCosts(const E_BuildingChoice &choice, Interface &interface) {
        m_Data[choice]->ShowCosts(interface);
    }
};