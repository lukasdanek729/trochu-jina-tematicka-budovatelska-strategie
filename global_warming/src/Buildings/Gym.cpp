#include "Gym.hpp"

Gym::Gym(FileReader & reader)
        : Building() {
    /// sets character for further saving and initialize values and icon from a file
    m_SaveCode = 'G';
    reader.OpenFile("src/Binaries/Gym.bin");
    reader.ReadIcone(m_Icon);
    ReadCommonParameters(reader);
    reader.Read(m_Cost_Money, sizeof(m_Cost_Money));
    reader.Read(m_Cost_Ice_Coffee, sizeof(m_Cost_Ice_Coffee));
    reader.Read(m_Incr_Money, sizeof(m_Incr_Money));
    reader.Read(m_Incr_Joy,   sizeof(m_Incr_Joy));
    reader.Read(m_Incr_Queers, sizeof(m_Incr_Queers));
    reader.Read(m_Limit_Money, sizeof(m_Limit_Money));
    reader.Read(m_Limit_Joy, sizeof(m_Limit_Joy));
    m_Limit_Sum_Money = m_Limit_Money;
    m_Limit_Sum_Joy = m_Limit_Joy;
}


void Gym::MultiplyAll () {
    Multiply(m_Incr_Money, m_Multipliers.m_Increment);
    Multiply(m_Incr_Joy, m_Multipliers.m_Increment);
    Multiply(m_Incr_Queers, m_Multipliers.m_Increment);
    Multiply(m_Limit_Money, m_Multipliers.m_Limit);
    Multiply(m_Limit_Joy, m_Multipliers.m_Limit);
    m_Limit_Sum_Money += m_Limit_Money;
    m_Limit_Sum_Joy += m_Limit_Joy;
    MultiplyCostsAndTime();
}

void Gym::MultiplyCostsAndTime () {
    Multiply(m_Cost_Money, m_Multipliers.m_Cost);
    Multiply(m_Cost_Ice_Coffee, m_Multipliers.m_Cost);

    Multiply(m_TimeForBuild, m_BuildMultiplier);
}

void Gym::ShowCosts(Interface & interface) {
    /// creates string containing current costs and passes it to interface
    std::string costs = "You need ";
    costs += std::to_string(m_Cost_Money);
    costs += " Money, ";
    costs += std::to_string(m_Cost_Ice_Coffee);
    costs += " Ice Coffee";
    interface.ShowInfo(&costs[0]);
}

bool Gym::Affordable (const Resources & resources) const {
    return resources.CheckValue({m_Cost_Money, m_Cost_Ice_Coffee},
                                Resources::MONEY | Resources::ICE_COFFEE );
}

void Gym::DeductCosts(Resources &resources) {
    resources.UpdateValues({m_Cost_Money, m_Cost_Ice_Coffee},
                           Resources::DEDUCT | Resources::MONEY | Resources::ICE_COFFEE);
}

void Gym::AddLimits(Resources & resources) {
    resources.UpdateLimit({m_Limit_Money, m_Limit_Joy},Resources::ADD | Resources::MONEY | Resources::JOY);
}
void Gym::DeductIncrements(Resources & resources) {
    resources.UpdateIncrement({m_Incr_Money, m_Incr_Joy}, Resources::DEDUCT | Resources::MONEY | Resources::JOY);
}
void Gym::AddIncrements(Resources & resources) {
    resources.UpdateIncrement({m_Incr_Money, m_Incr_Joy}, Resources::ADD | Resources::MONEY | Resources::JOY);
}

bool Gym::Destroy(Resources & resources) {
    /// building cannot be destroyed if it's unavailable
    if (!m_Available)
        return false;
    DeductIncrements(resources);
    resources.UpdateLimit({m_Limit_Sum_Money, m_Limit_Sum_Joy},
                          Resources::DEDUCT | Resources::MONEY | Resources::JOY);
    /// adds to resources generated values some part of building costs
    resources.UpdateValues({Remains(m_Cost_Money), Remains(m_Cost_Ice_Coffee)},
                           Resources::ADD | Resources::MONEY | Resources::ICE_COFFEE);
    return true;
}