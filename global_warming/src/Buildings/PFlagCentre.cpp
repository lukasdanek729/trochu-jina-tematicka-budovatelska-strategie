#include "PFlagCentre.hpp"

PFlagCentre::PFlagCentre(FileReader & reader)
        : Building() {
    /// sets character for further saving and initialize values and icon from a file
    m_SaveCode = 'F';
    reader.OpenFile("src/Binaries/PFlagCentre.bin");
    reader.ReadIcone(m_Icon);
    ReadCommonParameters(reader);
    reader.Read(m_Cost_Ice_Coffee, sizeof(m_Cost_Ice_Coffee));
    reader.Read(m_Cost_Bio_Cupcakes, sizeof(m_Cost_Bio_Cupcakes));
    reader.Read(m_Cost_Colors, sizeof(m_Cost_Colors));
    reader.Read(m_Cost_Queers, sizeof(m_Cost_Queers));
    reader.Read(m_Incr_Queers, sizeof(m_Incr_Queers));
}


void PFlagCentre::MultiplyAll () {
    Multiply(m_Incr_Queers, m_Multipliers.m_Increment);

    MultiplyCostsAndTime();
}

void PFlagCentre::MultiplyCostsAndTime () {
    Multiply(m_Cost_Ice_Coffee, m_Multipliers.m_Cost);
    Multiply(m_Cost_Bio_Cupcakes, m_Multipliers.m_Cost);
    Multiply(m_Cost_Colors, m_Multipliers.m_Cost);
    Multiply(m_Cost_Queers, m_Multipliers.m_Cost);

    Multiply(m_TimeForBuild, m_BuildMultiplier);
}


void PFlagCentre::ShowCosts(Interface & interface) {
    /// creates string containing current costs and passes it to interface
    std::string costs = "You need ";
    costs += std::to_string(m_Cost_Ice_Coffee);
    costs += " Ice Coffee, ";
    costs += std::to_string(m_Cost_Bio_Cupcakes);
    costs += " CupCakes, ";
    costs += std::to_string(m_Cost_Colors);
    costs += " Colors, ";
    costs += std::to_string(m_Cost_Queers);
    costs += " Queers";
    interface.ShowInfo(&costs[0]);
}

bool PFlagCentre::Affordable (const Resources & resources) const {
    return resources.CheckValue({m_Cost_Ice_Coffee, m_Cost_Bio_Cupcakes, m_Cost_Colors, m_Cost_Queers},
                                Resources::ICE_COFFEE | Resources::CUPCAKES | Resources::COLORS | Resources::QUEERS );
}

void PFlagCentre::DeductCosts(Resources &resources) {
    resources.UpdateValues({m_Cost_Ice_Coffee, m_Cost_Bio_Cupcakes, m_Cost_Colors, m_Cost_Queers},
                           Resources::DEDUCT | Resources::ICE_COFFEE | Resources::CUPCAKES | Resources::COLORS | Resources::QUEERS );
}

void PFlagCentre::AddLimits(Resources & resources) {
    /// permanently increases some Queer limit to Resources
    resources.UpdateLimit({m_Incr_Queers * 15}, Resources::ADD | Resources::QUEERS);
}
void PFlagCentre::DeductIncrements(Resources & resources) {
    resources.UpdateIncrement({m_Incr_Queers}, Resources::DEDUCT | Resources::QUEERS);
}
void PFlagCentre::AddIncrements(Resources & resources) {
    resources.UpdateIncrement({m_Incr_Queers}, Resources::ADD | Resources::QUEERS);
}

bool PFlagCentre::Destroy(Resources & resources) {
    /// building cannot be destroyed if it's unavailable
    if (!m_Available)
        return false;
    DeductIncrements(resources);
    /// adds to resources generated values some part of building costs
    resources.UpdateValues({Remains(m_Cost_Ice_Coffee), Remains(m_Cost_Bio_Cupcakes), Remains(m_Cost_Colors), Remains(m_Cost_Queers)},
                           Resources::ADD | Resources::ICE_COFFEE | Resources::CUPCAKES | Resources::COLORS | Resources::QUEERS);
    return true;
}