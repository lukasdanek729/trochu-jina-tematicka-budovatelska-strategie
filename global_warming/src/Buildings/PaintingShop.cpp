#include "PaintingShop.hpp"

PaintingShop::PaintingShop(FileReader & reader)
        : Building() {
    /// sets character for further saving and initialize values and icon from a file
    m_SaveCode = 'P';
    reader.OpenFile("src/Binaries/PaintingShop.bin");
    reader.ReadIcone(m_Icon);
    ReadCommonParameters(reader);
    reader.Read(m_Cost_Money, sizeof(m_Cost_Money));
    reader.Read(m_Cost_Joy, sizeof(m_Cost_Joy));
    reader.Read(m_Cost_Bio_Cupcakes, sizeof(m_Cost_Bio_Cupcakes));
    reader.Read(m_Cost_Queers, sizeof(m_Cost_Queers));
    reader.Read(m_Incr_Colors, sizeof(m_Incr_Colors));
    reader.Read(m_Limit_Colors, sizeof(m_Limit_Colors));
    m_Limit_Sum_Colors = m_Limit_Colors;
}


void PaintingShop::MultiplyAll () {
    Multiply(m_Incr_Colors, m_Multipliers.m_Increment);
    Multiply(m_Limit_Colors, m_Multipliers.m_Limit);
    m_Limit_Sum_Colors += m_Limit_Colors;

    MultiplyCostsAndTime();
}

void PaintingShop::MultiplyCostsAndTime () {
    Multiply(m_Cost_Money, m_Multipliers.m_Cost);
    Multiply(m_Cost_Joy, m_Multipliers.m_Cost);
    Multiply(m_Cost_Bio_Cupcakes, m_Multipliers.m_Cost);
    Multiply(m_Cost_Queers, m_Multipliers.m_Cost);

    Multiply(m_TimeForBuild, m_BuildMultiplier);
}

void PaintingShop::ShowCosts(Interface & interface) {
    /// creates string containing current costs and passes it to interface
    std::string costs = "You need ";
    costs += std::to_string(m_Cost_Money);
    costs += " Money, ";
    costs += std::to_string(m_Cost_Joy);
    costs += " Joy, ";
    costs += std::to_string(m_Cost_Bio_Cupcakes);
    costs += " CupCakes, ";
    costs += std::to_string(m_Cost_Queers);
    costs += " Queers";
    interface.ShowInfo(&costs[0]);
}

bool PaintingShop::Affordable (const Resources & resources) const {
    return resources.CheckValue({m_Cost_Money, m_Cost_Joy, m_Cost_Bio_Cupcakes, m_Cost_Queers},
                                Resources::MONEY | Resources::JOY | Resources::CUPCAKES | Resources::QUEERS );
}

void PaintingShop::DeductCosts(Resources &resources) {
    resources.UpdateValues({m_Cost_Money, m_Cost_Joy, m_Cost_Bio_Cupcakes, m_Cost_Queers},
                           Resources::DEDUCT | Resources::MONEY | Resources::JOY | Resources::CUPCAKES | Resources::QUEERS);
}

void PaintingShop::AddLimits(Resources & resources) {
    resources.UpdateLimit({m_Limit_Colors},Resources::ADD | Resources::COLORS);
}
void PaintingShop::DeductIncrements(Resources & resources) {
    resources.UpdateIncrement({m_Incr_Colors}, Resources::DEDUCT | Resources::COLORS);
}
void PaintingShop::AddIncrements(Resources & resources) {
    resources.UpdateIncrement({m_Incr_Colors}, Resources::ADD | Resources::COLORS);
}

bool PaintingShop::Destroy(Resources & resources) {
    /// building cannot be destroyed if it's unavailable
    if (!m_Available)
        return false;
    DeductIncrements(resources);
    resources.UpdateLimit({m_Limit_Sum_Colors},
                          Resources::DEDUCT | Resources::COLORS);
    /// adds to resources generated values some part of building costs
    resources.UpdateValues({Remains(m_Cost_Money), Remains(m_Cost_Joy), Remains(m_Cost_Bio_Cupcakes), Remains(m_Cost_Queers)},
                           Resources::ADD | Resources::MONEY | Resources::JOY | Resources::CUPCAKES | Resources::QUEERS);
    return true;
}