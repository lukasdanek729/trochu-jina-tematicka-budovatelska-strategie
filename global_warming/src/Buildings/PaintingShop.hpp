#pragma once

#include "Building.hpp"

/**
 * Painting shop class inherits Building class and implements every abstract method,
 * expands parameters according to type characteristics
 */
class PaintingShop : public Building {
protected:
    /**
     * Resources necessary for building and upgrading Adoption Centre
     */
    uint32_t m_Cost_Money;
    uint32_t m_Cost_Joy;
    uint32_t m_Cost_Bio_Cupcakes;
    uint32_t m_Cost_Queers;
    /**
     * Resources increment modified by Adoption Centre
     */
    uint32_t m_Incr_Colors;
    /**
     * Resource limit modified by Adoption Centre
     */
    uint32_t m_Limit_Colors;
    uint32_t m_Limit_Sum_Colors;

    /**
     * Multiplies every building parameter (costs, limits, increases and building time)
     */
    virtual void MultiplyAll() override;

    /**
     * Multiplies building costs and building time parameters
     */
    virtual void MultiplyCostsAndTime() override;

public:

    /**
     * Constructor of Adoption Centre
     * @param reader File reader for reading correct amounts of bytes and from correct file
     */
    PaintingShop(FileReader &reader);

    /**
     * Shows costs for building or upgrading building
     * @param interface Game interface where values are shown
     */
    virtual void ShowCosts(Interface &interface) override;

    /**
     * Finds out if game has enough resources for building or upgrading
     * @param resources Game resources for finding out their values
     * @return true if if game has enough resources for building or upgrading, false otherwise
     */
    virtual bool Affordable(const Resources &resources) const override;

    /**
     * Deduct game resources necessary for building or upgrading
     * @param resources Game resources for deducting their values
     */
    virtual void DeductCosts(Resources &resources) override;

    /**
     * Increases certain resources limits by building's update values
     * @param resources Game resources for updating their values
     */
    virtual void AddLimits(Resources &resources) override;

    /**
     * Decrease certain resources increment values by building's update values
     * @param resources Game resources for updating their values
     */
    virtual void DeductIncrements(Resources &resources) override;

    /**
     * Increases certain resources increment values by building's update values
     * @param resources Game resources for updating their values
     */
    virtual void AddIncrements(Resources &resources) override;

    /**
     * Adds some part of resources which were used for construction back to game before building is destroyed
     * @param resources Game resources where some values are slightly increased
     * @return true if destroying building is not in construction and resources were added, false otherwise
     */
    virtual bool Destroy(Resources &resources) override;

    /**
     * Duplicates building for separate use
     * @return unique pointer to newly created copy of building
     */
    virtual std::unique_ptr<Building> Clone() const override { return std::make_unique<PaintingShop>(*this); }

};