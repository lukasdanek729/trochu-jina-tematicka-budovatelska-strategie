#include "Residence.hpp"

Residence::Residence(FileReader & reader)
        : Building() {
    /// sets character for further saving and initialize values and icon from a file
    m_SaveCode = 'R';
    reader.OpenFile("src/Binaries/Residence.bin");
    reader.ReadIcone(m_Icon);
    ReadCommonParameters(reader);
    reader.Read(m_Cost_Money, sizeof(m_Cost_Money));
    reader.Read(m_Cost_Ice_Coffee, sizeof(m_Cost_Ice_Coffee));
    reader.Read(m_Cost_Bio_Cupcakes, sizeof(m_Cost_Bio_Cupcakes));
    reader.Read(m_Cost_Colors, sizeof(m_Cost_Colors));
    reader.Read(m_Inc_Queers, sizeof(m_Inc_Queers));
    reader.Read(m_Limit_Queers, sizeof(m_Limit_Queers));
    m_Limit_Sum_Queers = m_Limit_Queers;
}

bool Residence::CanUpgrade() const {
    return m_Level < (m_MaxLevel + 3);
}

void Residence::MultiplyAll () {
    Multiply(m_Inc_Queers, m_Multipliers.m_Increment);
    Multiply(m_Limit_Queers, m_Multipliers.m_Limit);
    m_Limit_Sum_Queers += m_Limit_Queers;

    MultiplyCostsAndTime();
}

void Residence::MultiplyCostsAndTime () {
    Multiply(m_Cost_Money, m_Multipliers.m_Cost);
    Multiply(m_Cost_Ice_Coffee, m_Multipliers.m_Cost);
    Multiply(m_Cost_Bio_Cupcakes, m_Multipliers.m_Cost);
    Multiply(m_Cost_Colors, m_Multipliers.m_Cost);

    Multiply(m_TimeForBuild, m_BuildMultiplier);
}

void Residence::ShowCosts(Interface & interface) {
    /// creates string containing current costs and passes it to interface
    std::string costs = "You need ";
    costs += std::to_string(m_Cost_Money);
    costs += " Money, ";
    costs += std::to_string(m_Cost_Ice_Coffee);
    costs += " Ice Coffee, ";
    costs += std::to_string(m_Cost_Bio_Cupcakes);
    costs += " CupCakes, ";
    costs += std::to_string(m_Cost_Colors);
    costs += " Colors";
    interface.ShowInfo(&costs[0]);
}

bool Residence::Affordable (const Resources & resources) const {
    return resources.CheckValue({m_Cost_Money, m_Cost_Ice_Coffee, m_Cost_Bio_Cupcakes, m_Cost_Colors},
                                Resources::MONEY | Resources::ICE_COFFEE | Resources::CUPCAKES | Resources::COLORS );
}

void Residence::DeductCosts(Resources &resources) {
    resources.UpdateValues({m_Cost_Money, m_Cost_Ice_Coffee, m_Cost_Bio_Cupcakes, m_Cost_Colors},
                           Resources::DEDUCT | Resources::MONEY | Resources::ICE_COFFEE | Resources::CUPCAKES | Resources::COLORS);
}

void Residence::AddLimits(Resources & resources) {
    resources.UpdateLimit({m_Limit_Queers},Resources::ADD | Resources::QUEERS);
}
void Residence::DeductIncrements(Resources & resources) {
    resources.UpdateIncrement({m_Inc_Queers}, Resources::DEDUCT | Resources::QUEERS);
}

void Residence::AddIncrements(Resources & resources) {
    resources.UpdateIncrement({m_Inc_Queers}, Resources::ADD | Resources::QUEERS);
}

bool Residence::Destroy(Resources & resources) {
    /// building cannot be destroyed if it's unavailable
    if (!m_Available)
        return false;
    resources.UpdateLimit({m_Limit_Sum_Queers},
                          Resources::DEDUCT | Resources::QUEERS);
    /// adds to resources generated values some part of building costs
    resources.UpdateValues({Remains(m_Cost_Money), Remains(m_Cost_Ice_Coffee), Remains(m_Cost_Bio_Cupcakes), Remains(m_Cost_Colors)},
                           Resources::ADD | Resources::MONEY | Resources::ICE_COFFEE | Resources::CUPCAKES | Resources::COLORS);
    return true;
}