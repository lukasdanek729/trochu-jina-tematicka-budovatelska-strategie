#include "StarBucks.hpp"

StarBucks::StarBucks(FileReader & reader)
        : Building() {
    /// sets character for further saving and initialize values and icon from a file
    m_SaveCode = 'S';
    reader.OpenFile("src/Binaries/StarBucks.bin");
    reader.ReadIcone(m_Icon);
    ReadCommonParameters(reader);
    reader.Read(m_Cost_Money, sizeof(m_Cost_Money));
    reader.Read(m_Cost_Joy, sizeof(m_Cost_Joy));
    reader.Read(m_Cost_Colors, sizeof(m_Cost_Colors));
    reader.Read(m_Incr_Ice_Coffee, sizeof(m_Incr_Ice_Coffee));
    reader.Read(m_Incr_Bio_Cupcakes, sizeof(m_Incr_Bio_Cupcakes));
    reader.Read(m_Limit_Ice_Coffee, sizeof(m_Limit_Ice_Coffee));
    reader.Read(m_Limit_Bio_Cupcakes, sizeof(m_Limit_Bio_Cupcakes));
    m_Limit_Sum_Bio_Cupcakes = m_Limit_Bio_Cupcakes;
    m_Limit_Sum_Ice_Coffee = m_Limit_Ice_Coffee;
}


void StarBucks::MultiplyAll () {
    Multiply(m_Incr_Ice_Coffee, m_Multipliers.m_Increment);
    Multiply(m_Incr_Bio_Cupcakes, m_Multipliers.m_Increment);
    Multiply(m_Limit_Ice_Coffee, m_Multipliers.m_Limit);
    Multiply(m_Limit_Bio_Cupcakes, m_Multipliers.m_Limit);
    m_Limit_Sum_Bio_Cupcakes += m_Limit_Bio_Cupcakes;
    m_Limit_Sum_Ice_Coffee += m_Limit_Ice_Coffee;

    MultiplyCostsAndTime();
}

void StarBucks::MultiplyCostsAndTime () {
    Multiply(m_Cost_Money, m_Multipliers.m_Cost);
    Multiply(m_Cost_Joy, m_Multipliers.m_Cost);
    Multiply(m_Cost_Colors, m_Multipliers.m_Cost);

    Multiply(m_TimeForBuild, m_BuildMultiplier);
}

void StarBucks::ShowCosts(Interface & interface) {
    /// creates string containing current costs and passes it to interface
    std::string costs = "You need ";
    costs += std::to_string(m_Cost_Money);
    costs += " Money, ";
    costs += std::to_string(m_Cost_Joy);
    costs += " Joy, ";
    costs += std::to_string(m_Cost_Colors);
    costs += " Colors";
    interface.ShowInfo(&costs[0]);
}

bool StarBucks::Affordable (const Resources & resources) const {
    return resources.CheckValue({m_Cost_Money, m_Cost_Joy, m_Cost_Colors},
                                Resources::MONEY | Resources::JOY | Resources::COLORS );
}

void StarBucks::DeductCosts(Resources &resources) {
    resources.UpdateValues({m_Cost_Money, m_Cost_Joy, m_Cost_Colors},
                           Resources::DEDUCT | Resources::MONEY | Resources::JOY | Resources::COLORS );
}

void StarBucks::AddLimits(Resources & resources) {
    resources.UpdateLimit({m_Limit_Ice_Coffee, m_Limit_Bio_Cupcakes},Resources::ADD | Resources::ICE_COFFEE | Resources::CUPCAKES);
}
void StarBucks::DeductIncrements(Resources & resources) {
    resources.UpdateIncrement({m_Incr_Ice_Coffee, m_Incr_Bio_Cupcakes}, Resources::DEDUCT | Resources::ICE_COFFEE | Resources::CUPCAKES);
}
void StarBucks::AddIncrements(Resources & resources) {
    resources.UpdateIncrement({m_Incr_Ice_Coffee, m_Incr_Bio_Cupcakes}, Resources::ADD | Resources::ICE_COFFEE | Resources::CUPCAKES);
}

bool StarBucks::Destroy(Resources & resources) {
    /// building cannot be destroyed if it's unavailable
    if (!m_Available)
        return false;
    DeductIncrements(resources);
    resources.UpdateLimit({m_Limit_Sum_Ice_Coffee, m_Limit_Sum_Bio_Cupcakes},
                          Resources::DEDUCT | Resources::ICE_COFFEE | Resources::CUPCAKES);
    /// adds to resources generated values some part of building costs
    resources.UpdateValues({Remains(m_Cost_Money), Remains(m_Cost_Joy),  Remains(m_Cost_Colors)},
                           Resources::ADD | Resources::MONEY | Resources::JOY | Resources::COLORS);
    return true;
}