#pragma once

#include <iostream>

/**
 * Coord struct for representation of building coordinates and map size in X and Y direction
 */
struct Coord {

    int16_t m_X = 0;
    int16_t m_Y = 0;

    /**
     * Comparing operator if coordinates are equal
     * @param left First coordinates to compare
     * @param right Second oordinates to compare
     * @return true if both coordinates are equal, false otherwise
     */
    friend bool operator==(const Coord &left, const Coord &right) {
        return (left.m_X == right.m_X) && (left.m_Y == right.m_Y);
    }

    /**
     * Comparing operator if one coordinate is smaller value than the other
     * @param left First coordinates to compare
     * @param right Second oordinates to compare
     * @return true if left (first) coordinates have smaller m_X value or m_X values are equal but first coordinates
     * has smaller m_Y value, false otherwise
     */
    friend bool operator<(const Coord &left, const Coord &right) {
        return (left.m_X < right.m_X) || ((left.m_X == right.m_X) && (left.m_Y < right.m_Y));
    }
};