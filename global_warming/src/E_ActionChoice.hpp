#pragma once
/**
 * Enum class of Actions for better code readability of user interactions
 */
enum class E_ActionChoice : uint8_t {
    BUILD   = '1',
    UPGRADE = '2',
    DESTROY = '3',
    SAVE    = '4',
    QUIT    = '5',
};