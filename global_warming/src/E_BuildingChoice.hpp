#pragma once
/**
 * Enum class of Building Types for better code readability of user building choice
 */
enum class E_BuildingChoice : uint8_t {
    ADOPT_CENTRE = '1',
    GYM          = '2',
    PAINT_SHOP   = '3',
    PFLAG_CENTRE = '4',
    STAR_BUCKS   = '5',
    RESIDENCE    = '6'
};