#pragma once
/**
 * Enum class of Difficulties for better code readability of user difficulty options
 */
enum class E_Difficulty {
    ANTIQUITY  = '1',
    THESE_DAYS = '2',
    MEDIEVAL   = '3'
};