#pragma once
/**
 * Enum class of Main Menu Chooices for better code readability when menu choices are used
 */
enum class E_MenuChoice : uint8_t {
    NEW_GAME  = '1',
    LOAD_GAME = '2',
    QUIT_GAME = '3'
};