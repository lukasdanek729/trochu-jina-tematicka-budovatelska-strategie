#include "Crusade.hpp"

Crusade::Crusade(FileReader & reader)
    : Event() {
    /// opens file with crusade visual and loads it
    reader.OpenFile("src/Binaries/Crusade.bin");
    reader.ReadVisual(m_Visual);
}

void Crusade::Generate (Resources & resources, Interface & interface) {
    /// generates random percentage penalization value and adjust resources by it
    m_Penalization = rand() % 10 + 1;
    resources.PercentageUpdateValues(m_Penalization, Resources::DEDUCT | Resources::ICE_COFFEE | Resources::JOY);
    m_Penalization++;
    resources.PercentageUpdateValues(m_Penalization, Resources::DEDUCT | Resources::MONEY);
    interface.ShowEvent(&m_Visual[0]);
    /// creates info about updated resources and shows it on terminal screen
    std::string info = "From resources were ";
    info += std::to_string(--m_Penalization);
    info += " %% of Ice Coffee and Joy plus ";
    info += std::to_string(++m_Penalization);
    info += " %% of Money deducted.";
    interface.ShowInfo(&info[0]);
}
