#pragma once

#include <iostream>
#include <vector>
#include <memory>

#include "../Resources.hpp"
#include "../Interface.hpp"

/**
 * Abstract Event class represents randomly generated game event
 * and defines all public methods which every events implements
 */
class Event {
protected:

    int16_t m_Penalization;

    /**
     * ASCII Art visualization
     */
    std::string m_Visual;

    /**
     * Private Event constructor
     */
    Event() = default;

public:

    /**
     * Event destructor
     */
    virtual ~Event() = default;

    /**
     * Generates event, modifies game resources and shows type of event and percentage addition or deduction
     * on cleared terminal screen
     * @param resources Resources which are updated
     * @param interface Interface where event is visualized
     */
    virtual void Generate(Resources &resources, Interface &interface) = 0;
};