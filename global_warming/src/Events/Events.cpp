#include "Events.hpp"

Events::Events(FileReader &reader)
        : m_PRIDE(reader) {
    /// fills unfortunate events
    m_UnfortunateEvents.push_back(std::make_unique<Crusade>(reader));
    m_UnfortunateEvents.push_back(std::make_unique<TraditionalFamilyRaid>(reader));
    m_UnfortunateEvents.push_back(std::make_unique<RainyDay>(reader));
}

void Events::SetIntervals(const E_Difficulty &difficulty) {
    /// sets interval for generating unfortunate events in range depending on game difficulty
    if (difficulty == E_Difficulty::MEDIEVAL)
        m_EventInterval = rand() % 10 + 10;
    else if (difficulty == E_Difficulty::THESE_DAYS)
        m_EventInterval = rand() % 20 + 10;
    else if (difficulty == E_Difficulty::ANTIQUITY)
        m_EventInterval = rand() % 30 + 10;
    m_Countdown = m_EventInterval;
}

void Events::LoadIntervals(FileReader &reader) {
    reader.Read(m_EventInterval, sizeof(m_EventInterval));
    reader.Read(m_Countdown, sizeof(m_Countdown));
}


bool Events::TryGenerate(Time &time, Resources &resources, Interface &interface) {
    /// if it's PRIDE time, generate PRIDE
    if (time.IsPride()) {
        m_PRIDE.Generate(resources, interface);
        interface.RefreshWindows();
        resources.Refresh(interface);
        time.Refresh(interface);
        return true;
    }
    /// checks if unfortunate event is generated
    if (--m_Countdown == 0) {
        m_Countdown = m_EventInterval;
        m_UnfortunateEvents[rand() % UNFORTUNATE_EVENT_COUNT]->Generate(resources, interface);
        interface.RefreshWindows();
        resources.Refresh(interface);
        time.Refresh(interface);
        return true;
    }
    return false;
}

void Events::SaveIntervals(FileWriter &writer) const {
    writer.Write(m_EventInterval, sizeof(m_EventInterval));
    writer.Write(m_Countdown, sizeof(m_Countdown));
}