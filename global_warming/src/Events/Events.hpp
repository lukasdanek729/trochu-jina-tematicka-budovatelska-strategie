#pragma once

#include <iostream>
#include <vector>
#include <memory>

#include "../FileReader.hpp"
#include "../FileWriter.hpp"
#include "../Time.hpp"
#include "../Resources.hpp"
#include "../Interface.hpp"

#include "Event.hpp"
#include "PRIDEFestival.hpp"
#include "Crusade.hpp"
#include "RainyDay.hpp"
#include "TraditionalFamilyRaid.hpp"

class Events {
private:

    int16_t m_EventInterval;
    int16_t m_Countdown;
    /**
     * PRIDE and Unfortunate events are separated for calling in different circumstances
     */
    PRIDEFestival m_PRIDE;
    std::vector<std::unique_ptr<Event>> m_UnfortunateEvents;

public:

    /**
     * Number of Unfortunate events available
     */
    static const uint8_t UNFORTUNATE_EVENT_COUNT = 3;

    /**
     * Constructor of Events with passed File reader for loading correct visuals
     * @param reader File reader for reading correct amounts of bytes and from correct file
     */
    Events(FileReader &reader);

    /**
     * Events destructor
     */
    ~Events() = default;

    /**
     * Sets intervals for unfortunate events according to passed game difficulty
     * @param difficulty Passed game difficulty
     */
    void SetIntervals(const E_Difficulty &difficulty);

    /**
     * Loads intervals from played game
     * @param reader File reader for reading correct amounts of bytes and from correct file
     */
    void LoadIntervals(FileReader &reader);

    /**
     * Tries to generate PRIDE or some of unfortunate events if correct amount of time passed
     * @param time Game time for checking PRIDE date
     * @param resources Game resources which are updated by generated event
     * @param interface Game interface for visualization of type of event and showing which resources were updated
     * @return
     */
    bool TryGenerate(Time &time, Resources &resources, Interface &interface);

    /**
     * Saves event intervals and countdown to a file
     * @param writer File writer for writing every resource variables to correct file
     */
    void SaveIntervals(FileWriter &fileWriter) const;

};