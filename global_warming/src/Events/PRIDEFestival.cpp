#include "PRIDEFestival.hpp"

PRIDEFestival::PRIDEFestival(FileReader &reader)
        : Event() {
    /// opens file with crusade visual and loads it
    reader.OpenFile("src/Binaries/PRIDEFestival.bin");
    reader.ReadVisual(m_Visual);
}

void PRIDEFestival::Generate(Resources &resources, Interface &interface) {
    /// generates random percentage addition value and adjust resources by it
    m_Penalization = rand() % 6 + 1;
    resources.PercentageUpdateValues(m_Penalization,
                                     Resources::ADD | Resources::JOY | Resources::COLORS | Resources::QUEERS);
    interface.ShowEvent(&m_Visual[0]);
    /// creates info about updated resources and shows it on terminal screen
    std::string info = "To resources were ";
    info += (m_Penalization + '0');
    info += " %% of Joy and Colors added and some Queers came out too.";
    interface.ShowInfo(&info[0]);
}
