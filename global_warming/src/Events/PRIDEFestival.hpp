#pragma once

#include <iostream>
#include <memory>

#include "Event.hpp"

/**
 * PRIDEFestival represents fortunate event, which adds some game resources
 */
class PRIDEFestival : public Event {
public:

    /**
     * Constructor of PRIDEFestival with passed File reader
     * @param reader File reader for reading correct amounts of bytes and from correct file
     */
    PRIDEFestival(FileReader &reader);

    /**
     * Generates event, modifies game resources and shows type of event and percentage addition or deduction
     * on cleared terminal screen
     * @param resources Resources which are updated
     * @param interface Interface where event is visualized
     */
    virtual void Generate(Resources &resources, Interface &interface) override;
};