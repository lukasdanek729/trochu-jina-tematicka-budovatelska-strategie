#include "RainyDay.hpp"

RainyDay::RainyDay(FileReader &reader)
        : Event() {
    /// opens file with rainy day visual and loads it
    reader.OpenFile("src/Binaries/RainyDay.bin");
    reader.ReadVisual(m_Visual);

}

void RainyDay::Generate(Resources &resources, Interface &interface) {
    /// generates random percentage penalization value and adjust resources by it
    m_Penalization = rand() % 5 + 1;
    resources.PercentageUpdateValues(m_Penalization, Resources::DEDUCT | Resources::CUPCAKES | Resources::COLORS);
    interface.ShowEvent(&m_Visual[0]);
    /// creates info about updated resources and shows it on terminal screen
    std::string info = "From resources were ";
    info += (m_Penalization + '0');
    info += " %% of CupCakes deducted and same percentage of Colors turned gray.";
    interface.ShowInfo(&info[0]);
}
