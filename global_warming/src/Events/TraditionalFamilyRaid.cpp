#include "TraditionalFamilyRaid.hpp"

TraditionalFamilyRaid::TraditionalFamilyRaid(FileReader &reader)
        : Event() {
    /// opens file with traditional family raid visual and loads it
    reader.OpenFile("src/Binaries/TraditionalFamilyRaid.bin");
    reader.ReadVisual(m_Visual);

}

void TraditionalFamilyRaid::Generate(Resources &resources, Interface &interface) {
    /// generates random percentage penalization value and adjust resources by it
    m_Penalization = rand() % 8 + 1;
    resources.PercentageUpdateValues(m_Penalization, Resources::DEDUCT | Resources::JOY | Resources::QUEERS);
    interface.ShowEvent(&m_Visual[0]);
    /// creates info about updated resources and shows it on terminal screen
    std::string info = "From resources were ";
    info += std::to_string(m_Penalization);
    info += " %% of Joy deducted and same percentage of Queers kidnapped.";
    interface.ShowInfo(&info[0]);
}
