#include "FileReader.hpp"

FileReader::FileReader()
        : m_Opened(false)
        , m_ReadFailed(false) {}

FileReader::~FileReader() {
    CloseFile();
}

void FileReader::CloseFile() {
    if (m_Opened) {
        m_ReadingFile.close();
        ///clearing file stream flags from previously opened file
        m_ReadingFile.clear();
    }
}

bool FileReader::OpenFile(const char * filePath) {
    CloseFile();
    m_ReadingFile.open(filePath, std::ios::in | std::ios::binary);
    return m_Opened = m_ReadingFile.good();
}

void FileReader::SetSlot (const int16_t & slot) {
    ///parsing game slot file path from entered number of game slot
    m_GameSlot  = "src/GameSlots/";
    m_GameSlot += std::to_string(slot);
    m_GameSlot += ".bin";
}
void FileReader::ReadString(std::string & str, const int16_t & lenght) {
    char letter;
    ///reading character by character from file, then pushing back to and of reading string
    for (int16_t i = 0; i < lenght + 1; ++i) {
        Read(letter, sizeof(char));
        if (m_ReadFailed)
            return;
        str.push_back(letter);
    }
}


void FileReader::ReadIcone(std::string &content) {
    ReadString(content, ICONE_HEIGHT * ICONE_WIDTH);
}

void FileReader::ReadVisual(std::string &content) {
    ReadString(content, VISUAL_HEIGHT * VISUAL_WIDTH);
}

E_Difficulty FileReader::ReadDifficulty () {
    ///Difficulty is first parameter to read from selected <gameslot>.bin, therefore opening correct file is necessary
    OpenFile(&m_GameSlot[0]);

    uint8_t difficulty;
    Read(difficulty, sizeof(difficulty));
    ///according to loaded character decides which Game difficulty to return
    switch (difficulty) {
        case '1':
            return E_Difficulty::ANTIQUITY;
        case '2':
            return E_Difficulty::THESE_DAYS;
        case '3':
            return E_Difficulty::MEDIEVAL;
        default:
            m_ReadFailed = true;
            return E_Difficulty::ANTIQUITY;
    }
}

E_BuildingChoice FileReader::ReadBuildingChoice() {
    uint8_t buildingChoice;
    Read(buildingChoice, sizeof(buildingChoice));
    ///according to loaded character decides which Building choice to return
    switch (buildingChoice) {
        case 'A':
            return E_BuildingChoice::ADOPT_CENTRE;
        case 'G':
            return E_BuildingChoice::GYM;
        case 'P':
            return E_BuildingChoice::PAINT_SHOP;
        case 'F':
            return E_BuildingChoice::PFLAG_CENTRE;
        case 'S':
            return E_BuildingChoice::STAR_BUCKS;
        case 'R':
            return E_BuildingChoice::RESIDENCE;
        default:
            m_ReadFailed = true;
            return E_BuildingChoice::ADOPT_CENTRE;
    }
}