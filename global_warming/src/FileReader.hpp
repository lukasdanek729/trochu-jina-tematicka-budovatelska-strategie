#pragma once

#include <iostream>
#include <string>
#include <fstream>

#include "E_Difficulty.hpp"
#include "E_BuildingChoice.hpp"

/**
 * FileReader class for taking care of opening, closing files, reading required types and checking success of reading
 */
class FileReader {
private:

    std::ifstream m_ReadingFile;
    bool          m_Opened;
    bool          m_ReadFailed;
    ///file path of selected game slot
    std::string   m_GameSlot;

    /**
     * Reads string with required length from file
     * @param str String which is read from a file
     * @param lenght Number of characters needed to be load
     */
    void ReadString(std::string &str, const int16_t &lenght);

public:
    /**
     * Enum representing dimensions of ASCII Art icone and visual
     */
    enum : int16_t {
        VISUAL_WIDTH = 80,
        VISUAL_HEIGHT = 26,
        ICONE_WIDTH = 9,
        ICONE_HEIGHT = 4
    };

    /**
     * Constructor of FileReader
     */
    FileReader();

    /**
     * Destructor of FileReader
     */
    ~FileReader();

    /**
     * Opens file at entered path
     * @param filePath Relative path to a file from /daneklu2 directory
     * @return
     */
    bool OpenFile(const char *filePath);

    /**
     * Closes currently opened file, if no file is open then does nothing
     */
    void CloseFile();

    /**
     * Checks if every reading was successful
     * @return true if every reading from file was successful, false otherwise
     */
    inline bool ReadFailed() const { return m_ReadFailed; }

    /**
     * Check if end of file is reached
     * @return true if file has reached it's end, false otherwise
     */
    inline bool IsEOF() { return m_ReadingFile.peek() == EOF; }

    /**
     * Getter for game slot path
     * @return full game slot path
     */
    inline std::string GetSlot() { return m_GameSlot; }

    /**
     * Setter for game slot path
     * @param slot Number of game slot for creating game slot path
     */
    void SetSlot(const int16_t &slot);

    /**
     * Fills data to simple data type from file
     * @tparam _Type Simple data type to be read
     * @param value Parameter to be filled  with value from file
     * @param size Number of bytes to be read, size of value too
     */
    template<class _Type>
    void Read(_Type &value, const uint32_t &size) {
        ///checks if reading is possible
        if (!m_Opened || !m_ReadingFile.good())
            m_ReadFailed = true;
        else
            m_ReadingFile.read((char *) &value, size);
    }

    /**
     * Reads Icone from a file
     * @param content Icone to be filled with ASCII Art
     */
    void ReadIcone(std::string &content);

    /**
     * Reads Visual from a file
     * @param content Visual to be filled with ASCII Art
     */
    void ReadVisual(std::string &content);

    /**
     * Reads difficulty from game slot
     * @return read difficulty
     */
    E_Difficulty ReadDifficulty();

    /**
     * Reads Building choice from game slot
     * @return read building choice
     */
    E_BuildingChoice ReadBuildingChoice();
};