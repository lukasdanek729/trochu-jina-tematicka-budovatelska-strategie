#include "FileWriter.hpp"

FileWriter::FileWriter(const std::string & gameSlot)
        : m_Opened(false)
        , m_WriteFailed (false)
        , m_GameSlot(gameSlot){}

FileWriter::~FileWriter() {
    CloseFile();
}

void FileWriter::CloseFile() {
    if (m_Opened) {
        m_WritingFile.close();
        ///clearing file stream flags from previously opened file
        m_WritingFile.clear();
    }
}

void FileWriter::OpenFile(const char * filePath) {
    CloseFile();
    m_WritingFile.open(filePath, std::ios::out | std::ios::binary);
    m_Opened = m_WritingFile.good();
}

void FileWriter::WriteChar (const char & value) {
    ///checks if writing a character is possible
    if (m_Opened && m_WritingFile.good())
        m_WritingFile.write((char*)&value, sizeof(char));
    else
        m_WriteFailed = true;
}

void FileWriter::WriteDifficulty(const E_Difficulty &difficulty) {
    ///Difficulty is first parameter written to a selected <gameslot>.bin, therefore opening correct file is necessary
    OpenFile(&m_GameSlot[0]);
    ///according to passed difficulty decides which character to write
    switch (difficulty) {
        case E_Difficulty::ANTIQUITY:
            WriteChar('1');
            break;
        case E_Difficulty::THESE_DAYS:
            WriteChar('2');
            break;
        case E_Difficulty::MEDIEVAL:
            WriteChar('3');
            break;
        default:
            m_WriteFailed = true;
            break;
    }
}
