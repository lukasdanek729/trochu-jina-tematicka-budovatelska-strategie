#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include "E_Difficulty.hpp"

/**
 * FileWriter class for taking care of opening, closing files, writing required types and checking success of writing
 */
class FileWriter {
private:

    std::ofstream m_WritingFile;
    bool          m_Opened;
    bool          m_WriteFailed;
    ///file path of selected game slot
    std::string   m_GameSlot;

public:
    /**
     * Constructor with game slot to write in
     * @param gameSlot Relative file path from /daneklu2 to game slot whe data are going to be saved
     */
    FileWriter(const std::string &gameSlot);

    /**
     * Destructor of FileWriter
     */
    ~FileWriter();

    /**
     * Opens file at entered path for writing
     * @param filePath Relative path to a file from /daneklu2 directory
     */
    void OpenFile(const char *filePath);

    /**
     * Closes currently opened file, if no file is open then does nothing
     */
    void CloseFile();

    /**
     * Checks if some writing to file failed
     * @return
     */
    inline bool WriteFailed() const { return m_WriteFailed; }

    /**
     * Writes one character to file
     * @param value Charcter to be written to file
     */
    void WriteChar(const char &value);

    /**
     * Writes simple data type to file
     * @tparam _Type Simple data type to be written
     * @param value Parameter to be written to file
     * @param size Number of bytes to be written, size of value too
     */
    template<class _Type>
    void Write(_Type &value, const size_t &size) {
        if (!m_Opened || !m_WritingFile.good())
            m_WriteFailed = true;
        else
            m_WritingFile.write((char *) &value, size);
    }

    /**
     * Writes difficulty to game slot
     * @param difficulty Difficulty to be written
     */
    void WriteDifficulty(const E_Difficulty &difficulty);
};