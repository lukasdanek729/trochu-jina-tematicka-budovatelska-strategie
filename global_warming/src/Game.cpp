#include "Game.hpp"

Game::Game(FileReader &reader)
        : m_FileWriter(reader.GetSlot()),
          m_Events(reader),
          m_Buildings(reader),
          m_Running(true),
          m_Initialized(true),
          m_Difficulty(reader.ReadDifficulty()),
          m_Time(reader),
          m_Resources(reader),
          m_Map(reader) {
    if (reader.ReadFailed())
        m_Initialized = false;
    else {
        m_Events.LoadIntervals(reader);
        LoadBuildings(reader);
    }
}

Game::Game(FileReader &reader, const E_Difficulty &difficulty, const Coord &size)
        : m_FileWriter(reader.GetSlot()),
          m_Events(reader),
          m_Buildings(reader),
          m_Running(true),
          m_Initialized(true),
          m_Difficulty(difficulty),
          m_Time(difficulty),
          m_Resources(difficulty),
          m_Map(size) {
    m_Events.SetIntervals(m_Difficulty);
    m_Initialized = !reader.ReadFailed();
}

void Game::LoadBuildings(FileReader &reader) {
    E_BuildingChoice choice;
    Coord coords;
    while (!reader.IsEOF()) {
        reader.Read(coords.m_X, sizeof(coords.m_X));
        reader.Read(coords.m_Y, sizeof(coords.m_Y));
        choice = reader.ReadBuildingChoice();
        if (reader.ReadFailed() || !m_Map.Insert(coords, m_Buildings.CloneBuilding(choice))) {
            m_Initialized = false;
            break;
        }
        m_Map.MakeAvailable(coords)->Load(reader);
    }
}

bool Game::Save() const {
    m_FileWriter.WriteDifficulty(m_Difficulty);
    m_Time.Save(m_FileWriter);
    m_Resources.Save(m_FileWriter);
    m_Map.SaveDimensions(m_FileWriter);
    m_Events.SaveIntervals(m_FileWriter);
    m_Map.SaveContent(m_FileWriter);
    ///closes file after every parameter necessary for further load is written
    m_FileWriter.CloseFile();
    return !m_FileWriter.WriteFailed();
}

void Game::Update(Interface &interface) {
    ///checking if user didn't resize terminal
    if (interface.Resized()) {
        Save();
        m_Running = false;
        return;
    }
    m_Time.Update(interface);
    ///checks if some event is generated, if not updates resources
    if (!m_Events.TryGenerate(m_Time,m_Resources, interface)) {
        if (!m_Resources.Generate(interface)) {
            m_Running = false;
            return;
        }
    }
    m_Map.Update(m_Resources, interface);
}

void Game::ProcessInput(Interface &interface) {
    /// on user's input decides which method call or does nothing
    switch (interface.ChooseAction()) {
        case E_ActionChoice::BUILD: {
            E_BuildingChoice choice = interface.ChooseBuilding();
            Coord coords = interface.ChooseCoords();
            m_Buildings.ShowCosts(choice, interface);
            if (!Build(coords, choice))
                interface.ShowError("Cannot build on these coordinates");
            break;
        }
        case E_ActionChoice::UPGRADE: {
            if (!Upgrade(interface.ChooseCoords(), interface))
                interface.ShowError("Cannot upgrade building on these coordinates");
            break;
        }
        case E_ActionChoice::DESTROY: {
            Coord coords = interface.ChooseCoords();
            if (!Destroy(coords))
                interface.ShowError("Cannot destroy building on these coordinates");
            else
                interface.ClearMapCell(coords);
            break;
        }
        case E_ActionChoice::SAVE: {
            if (!Save())
                interface.ShowError("Cannot save progress right now.");
            break;
        }
        case E_ActionChoice::QUIT: {
            if (!Save())
                interface.ShowError("Cannot save progress right now.");
            m_Running = false;
            break;
        }
        default:
            break;
    }
    interface.RefreshWindows();
}

bool Game::Build(const Coord &coords, E_BuildingChoice &choice) {
    ///checks correct coordinates and if building could be inserted to map and then build
    if (!m_Map.InBorders(coords) ||
        !m_Buildings.Affordable(choice, m_Resources) ||
        m_Map.PositionFound(coords) ||
        !m_Map.Insert(coords, m_Buildings.CloneBuilding(choice)))
        return false;
    ///checks if building can be finally build
    return m_Map.MakeAvailable(coords)->Build(m_Resources);
}

bool Game::Upgrade(const Coord &coords, Interface &interface) {
    ///checks correct coordinates
    if (!m_Map.InBorders(coords) || !m_Map.PositionFound(coords))
        return false;
    /// if max level was reached dd not show upgrade costs
    if (m_Map.MakeAvailable(coords)->CanUpgrade()) {
        m_Map.MakeAvailable(coords)->ShowCosts(interface);
    }
    ///checks if building can be upgraded
    return m_Map.MakeAvailable(coords)->Upgrade(m_Resources);
}

bool Game::Destroy(const Coord &coords) {
    ///checks correct coordinates
    if (!m_Map.InBorders(coords) || !m_Map.PositionFound(coords))
        return false;
    ///checks if building could be destroyed
    if (!m_Map.MakeAvailable(coords)->Destroy(m_Resources))
        return false;
    ///checks if building was erased from map
    return m_Map.Delete(coords);
}