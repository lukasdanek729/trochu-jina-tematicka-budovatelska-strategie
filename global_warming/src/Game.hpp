#pragma once

#include <iostream>

#include "FileReader.hpp"
#include "FileWriter.hpp"
#include "Coord.hpp"
#include "Time.hpp"
#include "Map.hpp"
#include "Resources.hpp"
#include "Interface.hpp"
#include "Events/Events.hpp"
#include "Buildings/Buildings.hpp"

#include "E_Difficulty.hpp"
#include "E_BuildingChoice.hpp"
#include "E_ActionChoice.hpp"

/**
 * Game class represents whole game logic with all assets and possibility to save progress
 */
class Game {
private:
            ///necessary for saving state of game for further loading
    mutable FileWriter   m_FileWriter;
            ///game events and buildings generated in game
            Events       m_Events;
            Buildings    m_Buildings;
            ///flags for checing current state of game
            bool         m_Running;
            bool         m_Initialized;
            ///game objects which are changed during play time
            E_Difficulty m_Difficulty;
            Time         m_Time;
            Resources    m_Resources;
            Map          m_Map;

    /**
     * Loads every constructed building from game slot from which is game being loaded
     * @param reader FileReader for taking care of correct file reading
     */
    void LoadBuildings(FileReader &reader);

public:
    /**
     * Game constructor for constructing played game from game slot
     * @param reader FileReader for taking care of correct file reading
     */
    Game(FileReader &reader);

    /**
     * Game constructor for constructing new game
     * @param reader FileReader for taking care of correct events and buildings reading from files
     * @param difficulty Game difficulty which player chose
     * @param size Game map size for creating game map
     */
    Game(FileReader &reader, const E_Difficulty &difficulty, const Coord &size);

    /**
     * Game destructor
     */
    ~Game() = default;

    /**
     * Game cannot be copied
     */
    Game(const Game &src) = delete;

    Game &operator=(const Game &src) = delete;

    /**
     * Game cannot be moved
     */
    Game(Game &&src) = delete;

    Game &operator=(Game &&src) = delete;

    /**
     * Checks if game is still running
     * @return true if game is still running, false otherwise
     */
    inline bool IsRunning() const { return m_Running; }

    /**
     * Checks if game was correctly initialized
     * @return true if game was correctly initialized, false otherwise
     */
    inline bool IsInitialized() const { return m_Initialized; }

    /**
     * Sets correct aligns in user interface
     * @param interface User interface where game progress is visualized
     */
    inline void SetInterface(Interface &interface) const {
        interface.SetAllWindows(FileReader::ICONE_WIDTH, FileReader::ICONE_HEIGHT, m_Map.GetDimensions(),
                                FileReader::VISUAL_WIDTH, FileReader::VISUAL_HEIGHT);
    }

    /**
     * Checks if loaded game can fit terminal size
     * @param interface User interface where game progress is visualized
     * @return true if loaded game can fit terminal size, false otherwise
     */
    inline bool CanShowWholeMap(Interface & interface) const { return interface.CanFitMap (m_Map.GetDimensions()); }

    /**
     * Saves game progress to game slot picked before game initialization
     * @return true if saving progress was successful, false otherwise
     */
    bool Save() const;

    /**
     * Updates game state, like resources values, construction time of buildings and time and checks if some event
     * could be generated
     * @param interface User interface where game progress is visualized
     */
    void Update(Interface &interface);

    /**
     * Catches user input and calls correct method for taking care of it.
     * @param interface User interface where game progress is visualized
     */
    void ProcessInput(Interface &interface);

    /**
     * Tries to build selected type of building on selected coordinates on map
     * @param coords Building coordinates where should be build
     * @param choice Building type which user chose
     * @return true if constructing and putting into map was successful, false otherwise
     */
    bool Build(const Coord &coords, E_BuildingChoice &choice);

    /**
     * Tries to upgrade  building on selected coordinates on map
     * @param coords Building coordinates where building to upgrade should stand
     * @param interface User interface where game progress is visualized
     * @return true if building was found and upgrading was successful, false otherwise
     */
    bool Upgrade(const Coord &coords, Interface &interface);

    /**
     * Tries to destroy building on selected coordinates on map
     * @param coords Building coordinates where building to be destroyed should stand
     * @return true if building was found and destroying was successful, false otherwise
     */
    bool Destroy(const Coord &coords);
};