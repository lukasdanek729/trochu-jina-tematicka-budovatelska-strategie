#include "Interface.hpp"

Interface::Interface (){
    initscr();
    noecho();
    start_color();
    m_Cols  = COLS;
    m_Lines = LINES;
}

Interface::~Interface() {
    endwin();
}

void Interface::Initialize() {
    mvprintw(0, 0, "Type \"1\" for basic colors and \"2\" for extended colors");
    uint8_t colorPalette = GetASCIINumber('2');
    if (colorPalette == '2') {
        init_pair(1, COLOR_BLACK, RED);
        init_pair(2, COLOR_BLACK, ORANGE);
        init_pair(3, COLOR_BLACK, YELLOW);
        init_pair(4, COLOR_BLACK, GREEN);
        init_pair(5, COLOR_BLACK, BLUE);
        init_pair(6, COLOR_BLACK, MAGENTA);
        init_pair(7, COLOR_BLACK, BACKGROUND);
        init_pair(8, COLOR_BLACK, WHITE);
        init_pair(9, COLOR_BLACK, GRAY);
    }
    else {
        init_pair(1, COLOR_BLACK, COLOR_RED);
        init_pair(2, COLOR_BLACK, COLOR_YELLOW);
        init_pair(3, COLOR_BLACK, COLOR_GREEN);
        init_pair(4, COLOR_RED  , COLOR_YELLOW);
        init_pair(5, COLOR_WHITE, COLOR_BLUE);
        init_pair(6, COLOR_WHITE, COLOR_MAGENTA);
        init_pair(7, COLOR_WHITE, COLOR_BLACK);
        init_pair(8, COLOR_BLACK, COLOR_WHITE);
        init_pair(9, COLOR_BLACK, COLOR_BLACK);
    }
    bkgd(COLOR_PAIR(7));
    CheckTerminalSize();
}

void Interface::CheckTerminalSize() {
    if (COLS < MIN_COLS || LINES < MIN_LINES)
        ShowError("Terminal size is too small, close application and resize it.");
}

void Interface::SetAllWindows (const int16_t & mapCellWidth, const int16_t & mapCellHeight, const Coord & mapSize,
                               const int16_t & eventWidth,   const int16_t & eventHeight) {
    SetResourcesWindows();
    SetDateWindow();
    SetEventWindow(eventWidth, eventHeight);
    SetMapWindows(mapCellWidth, mapCellHeight, mapSize);
    refresh();
    RefreshWindows();
}

void Interface::SetResourcesWindows() {
    m_ResourcesWindows.clear();
    ///Create main window for resources
    WINDOW *resourcesMainWin = newwin(6, COLS, 1, 0);
    m_ResourcesWindows.push_back(resourcesMainWin);
    ///Set names of resources
    m_ResourcesNames = {"Money     ", "Ice Coffee", "CupCakes  ", "Joy       ", "Colors    ", "Queers    "};
    ///Devide columns by two (bit shift right) and then deduct 3 times size of Resource Window
    int16_t xWinPosition = (COLS >> 1) - 36;
    ///Creates all windows for resources
    for (int16_t i = 1; i <= 6; ++i) {
        WINDOW *resourceWin = derwin(m_ResourcesWindows[0], 4, 12, 1, xWinPosition);
        m_ResourcesWindows.push_back(resourceWin);
        xWinPosition += 12;
    }
}

void Interface::SetMapWindows(const int16_t & mapCellWidth, const int16_t & mapCellHeight, const Coord & mapSize) {
    /// sets corresponding map and icone values for current game
    m_Map_Width = mapSize.m_X;
    m_Map_Height = mapSize.m_Y;
    m_Icon_Width = mapCellWidth;
    m_Icon_Height = mapCellHeight;
    /// clears previous game map
    m_MapWindows.clear();
    m_MainMapWindow = newwin(LINES - 8, COLS, 8, 0);
    /// creates map matrix with correct aligns on sides
    int16_t xMapCellPos = (COLS >> 1) - ((m_Map_Width * (m_Icon_Width + 1) + 1) >> 1);
    int16_t yMapCellPos = ((LINES - 8) >> 1) - ((m_Map_Height * (m_Icon_Height + 1) + 1) >> 1);
    for (int16_t y = 0; y < m_Map_Height; ++y) {
        std::vector<WINDOW *> mapRow;
        int16_t rowXPos = xMapCellPos;
        for (int16_t x = 0; x < m_Map_Width; ++x) {
            WINDOW *mapCell = derwin(m_MainMapWindow, m_Icon_Height, m_Icon_Width, yMapCellPos, rowXPos);
            mapRow.push_back(mapCell);
            rowXPos += (m_Icon_Width + 1);
        }
        m_MapWindows.push_back(mapRow);
        yMapCellPos += (m_Icon_Height + 1);
    }
}
void Interface::SetEventWindow(const int16_t & eventWidth,   const int16_t & eventHeight) {
    int16_t xPos = (COLS  - eventWidth) >> 1;
    int16_t yPos = (LINES - eventHeight) >> 1;
    m_EventWindow = newwin(eventHeight, eventWidth, yPos, xPos);
}

void Interface::RefreshWindows() {
    /// clears screen and sets game title
    clear();
    mvprintw(0, (COLS >> 1) - 7, "Global Warming");
    /// refreshes main resources window
    wbkgd(m_ResourcesWindows[0], COLOR_PAIR(7));
    mvwprintw(m_ResourcesWindows[0], 0, 1, "Resources");
    int16_t xWinPosition = (COLS >> 1) - 36;
    mvwprintw(m_ResourcesWindows[0], 2, xWinPosition - 10, "Generated");
    mvwprintw(m_ResourcesWindows[0], 3, xWinPosition - 10, "    Limit");
    refresh();
    wrefresh(m_ResourcesWindows[0]);
    /// refreshes date window with right color
    wbkgd(m_DateWindow, COLOR_PAIR(8));
    wrefresh(m_DateWindow);
    /// refreshes map position
    wbkgd(m_MainMapWindow, COLOR_PAIR(7));
    mvwprintw(m_MainMapWindow, 0, 1, "Map");
    /// refreshes every map cell and it's coordinates
    int16_t xMapCellPos = (COLS >> 1) - ((m_Map_Width * (m_Icon_Width + 1) + 1) >> 1);
    int16_t  yMapCellPos = ((LINES - 8) >> 1) - ((m_Map_Height * (m_Icon_Height + 1) + 1) >> 1);
    for (int16_t  y = 0; y < m_Map_Height; ++y) {
        int16_t  rowXPos = xMapCellPos;
        for (int16_t  x = 0; x < m_Map_Width; ++x) {
            mvwprintw(m_MainMapWindow, yMapCellPos + 4, rowXPos, "%d %d", x, y);
            wbkgd(m_MapWindows[y][x], COLOR_PAIR(8));
            rowXPos += 10;
        }
        yMapCellPos += 5;
    }
    refresh();
    wrefresh(m_MainMapWindow);
}

uint8_t Interface::GetASCIINumber(const uint8_t &maxValue) {
    noecho();
    m_CharBuffer = '\0';
    /// checks minimal and maximal ASCII value of entered number
    while (m_CharBuffer > maxValue || m_CharBuffer < '0') {
        timeout(-1);
        m_CharBuffer = getch();
    }
    return m_CharBuffer;
}

bool Interface::GetTwoNumbers(int16_t & first, int16_t & second) {
    /// sets user input visualization on, reads 10 characters and rest is flushed
    echo();
    char input[BUFFER_SIZE];
    getnstr(input, 10);
    flushinp();
    noecho();
    /// sets both output number to zero and tries to parse them
    first = second = 0;
    int16_t i = 0;
    /// loads numbers until first white space
    while (input[i] != ' ') {
        if (input[i] > '9'|| input[i] < '0')
            return false;
        else
            first = first * 10 + (input[i] - '0');
        ++i;
    }
    /// skips white space position
    ++i;
    while (input[i] != '\0') {
        if (input[i] > '9'|| input[i] < '0')
            return false;
        else
        second = second * 10 + (input[i] - '0');
        ++i;
    }
    return true;
}

void Interface::ShowChoices(const std::vector<const char *> & choices) {
    clear();
    m_ChoicesWindows.clear();
    int16_t numberOfChoices = choices.size();
    int16_t yWinPosition = (LINES - (numberOfChoices * CHOICE_Y_SIZE)) / 2;
    int16_t xTextPosition = COLS / 3;

    for (int16_t i = 1; i <= numberOfChoices; ++i) {
        WINDOW * choice = newwin(CHOICE_Y_SIZE, COLS, yWinPosition, 0);
        wbkgd(choice, COLOR_PAIR(i));
        mvwprintw(choice, 1, xTextPosition - 4, "(%d)", i);
        mvwprintw(choice, 1, xTextPosition, choices[i - 1]);
        m_ChoicesWindows.push_back(choice);
        yWinPosition += CHOICE_Y_SIZE;
    }

    refresh();
    for (auto & m_ChoicesWindow : m_ChoicesWindows)
        wrefresh(m_ChoicesWindow);
}

void Interface::ShowMessage(const char * message, const bool & error) {
    clear();
    int16_t size = strlen(message);
    int16_t messageXPos = (COLS >> 1) - (size >> 1);
    WINDOW * messageWin = newwin(LINES >> 1, COLS, LINES >> 2 , 0);

    if (error)
        wbkgd(messageWin, COLOR_PAIR(1));
    else
        wbkgd(messageWin, COLOR_PAIR(5));


    mvwprintw(messageWin, LINES >> 2, messageXPos, message);
    refresh();
    wrefresh(messageWin);
}

bool Interface::EnterPressed() {
    timeout(0);
    m_CharBuffer = getch();

    if (m_CharBuffer == '\n')
        return true;
    else {
        flushinp();
        return false;
    }
}

void Interface::WaitForEnter() {
    m_CharBuffer = '\0';
    while (m_CharBuffer != '\n') {
        timeout(-1);
        m_CharBuffer = getch();
    }
}

E_MenuChoice Interface::ChooseInMainMenu() {
    ShowChoices ({"New Game", "Load Game", "Quit"});
    uint8_t value;
    /// value can be value only 1 to 3, therefore static_cast can always cast value to correct E_MenuChoice
    value = GetASCIINumber('3');
    return static_cast<E_MenuChoice>(value);
}

int16_t Interface::ChooseGameSlot() {
    ShowChoices ({"Slot 1", "Slot 2", "Slot 3", "Slot 4", "Slot 5", "Slot 6"});
    uint8_t value;
    value = GetASCIINumber('6');
    return (int16_t)value - '0';
}

E_Difficulty Interface::ChooseDifficulty () {
    ShowChoices ({"Antiquity", "These Days", "Medieval"});
    uint8_t value;
    /// value can be value only 1 to 3, therefore static_cast can always cast value to correct E_Difficulty
    value = GetASCIINumber('3');
    return static_cast<E_Difficulty>(value);
}

Coord Interface::ChooseMapSize () {
    Coord size;
    int16_t maxWidth = (COLS - 3 ) / 10;
    int16_t maxHeight = (LINES - 10) / 5;
    while (true) {
        ShowMessage("Enter map width (minimum is 8) and height(minimum is 4)", false);

        if (!GetTwoNumbers(size.m_X, size.m_Y))
            ShowError("Wrong Input!");
        else if (size.m_X < MIN_MAP_WIDTH || size.m_Y < MIN_MAP_HEIGHT)
            ShowError("Too small map size entered!");
        else if (size.m_X > maxWidth || size.m_Y > maxHeight)
            ShowError("Too big map size entered!");
        else
            break;
    }
    return size;
}

E_ActionChoice Interface::ChooseAction() {
    ShowChoices ({"Build", "Upgrade", "Destroy", "Save", "Quit", "Cancel"});
    uint8_t value;
    value = GetASCIINumber('6');
    /// value can be value only 1 to 6, therefore static_cast can always cast value to correct E_ActionChoice
    return static_cast<E_ActionChoice>(value);

}

E_BuildingChoice Interface::ChooseBuilding() {
    ShowChoices ({"Adoption Centre", "Gym", "Painting Shop",
                         "P-Flaf Centre", "Star Bucks", "Residence"});
    uint8_t value;
    value = GetASCIINumber('6');
    /// value can be value only 1 to 6, therefore static_cast can always cast value to correct E_BuildingChoice
    return static_cast<E_BuildingChoice>(value);
}

Coord Interface::ChooseCoords() {
    Coord coords;
    /// until valid two numbers are entered, shows Error message
    while (true) {
        ShowMessage("Type two numbers separated with [SPACE] representing x and y coordinate", false);
        if (!GetTwoNumbers(coords.m_X, coords.m_Y))
            ShowError("Wrong Input!");
        else
            break;
    }
    return coords;
}

void Interface::UpdateDate(const int16_t &day, const int16_t &month, const int16_t &year) {
    wbkgd(m_DateWindow, COLOR_PAIR(8));
    int16_t xDatePosition = (COLS >> 1) - 6;
    mvwprintw(m_DateWindow, 0, xDatePosition, "Date: %02d.%02d.%04d", day, month, year);
    wrefresh(m_DateWindow);}

void Interface::UpdateResource(const int16_t & value, const int16_t & limit, const int16_t & fieldNumber) {
    wbkgd(m_ResourcesWindows[fieldNumber], COLOR_PAIR(fieldNumber));
    mvwprintw(m_ResourcesWindows[fieldNumber], 0, 1, m_ResourcesNames[fieldNumber - 1]);
    mvwprintw(m_ResourcesWindows[fieldNumber], 1, 1, "%10u", value);
    mvwprintw(m_ResourcesWindows[fieldNumber], 2, 1, "%10u", limit);
    wrefresh(m_ResourcesWindows[fieldNumber]);
}

void Interface::UpdateMapCell(const Coord & coords, const char * image, const int16_t & levelColor) {
    /// checks passed color level if level is not below 1, then turns cell gray or above 6, then keeps level color to 6
    if (levelColor > 6)
        wbkgd(m_MapWindows[coords.m_Y][coords.m_X], COLOR_PAIR(6));
    else if (levelColor < 1)
        wbkgd(m_MapWindows[coords.m_Y][coords.m_X], COLOR_PAIR(9));
    else
        wbkgd(m_MapWindows[coords.m_Y][coords.m_X], COLOR_PAIR(levelColor));
    mvwprintw(m_MapWindows[coords.m_Y][coords.m_X], 0, 0 , image);
    wrefresh(m_MapWindows[coords.m_Y][coords.m_X]);
}

void Interface::ClearMapCell(const Coord & coords) {
    wclear(m_MapWindows[coords.m_Y][coords.m_X]);
    wbkgd(m_MapWindows[coords.m_Y][coords.m_X], COLOR_PAIR(8));
}
void Interface::ShowEvent(const char * visual) {
    clear();
    wbkgd(m_EventWindow, COLOR_PAIR(7));
    mvwprintw(m_EventWindow, 0, 0, visual);
    refresh();
    wrefresh(m_EventWindow);
    WaitForEnter();
}

void Interface::ShowInfo(const char * info) {
    ShowMessage(info, false);
    WaitForEnter();
}

void Interface::ShowError(const char * message) {
    ShowMessage(message, true);
    WaitForEnter();
}


bool Interface::Resized() {
    if (m_Cols != COLS || m_Lines != LINES) {
        m_Cols = COLS;
        m_Lines = LINES;
        ShowInfo("Terminal Resized! Restart game.");
        return true;
    }
    else
        return false;
}