#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <curses.h>
#include <vector>

#include "Coord.hpp"
#include "E_MenuChoice.hpp"
#include "E_Difficulty.hpp"
#include "E_ActionChoice.hpp"
#include "E_BuildingChoice.hpp"

/**
 * Interface class for visualization of game progress and for catching user inputs
 */
class Interface {
private:
    /// buffer for one read character from input
    uint8_t                           m_CharBuffer;
    /// current size of terminal
    int16_t                           m_Cols;
    int16_t                           m_Lines;
    /// loaded ASCII Art dimensions
    int16_t                           m_Icon_Width;
    int16_t                           m_Icon_Height;
    int16_t                           m_Map_Width;
    int16_t                           m_Map_Height;
    /// used names of resources for constant refreshing
    std::vector<const char *>         m_ResourcesNames;
    /// windows for game elements visualization
    WINDOW *                          m_DateWindow;
    WINDOW *                          m_MainMapWindow;
    WINDOW *                          m_EventWindow;
    std::vector<WINDOW*>              m_ChoicesWindows;
    std::vector<WINDOW*>              m_ResourcesWindows;
    std::vector<std::vector<WINDOW*>> m_MapWindows;

    /// constants to maintain at least the minimum size of map and terminal size
    static const int16_t MIN_COLS       = 84;
    static const int16_t MIN_LINES      = 30;
    static const int16_t MIN_MAP_HEIGHT = 4;
    static const int16_t MIN_MAP_WIDTH  = 8;
    static const int16_t CHOICE_Y_SIZE  = 3;
    static const int16_t BUFFER_SIZE    = 10;

    /// constatns of terminal colors
    static const int RED        = 160;
    static const int ORANGE     = 208;
    static const int YELLOW     = 226;
    static const int GREEN      = 34;
    static const int BLUE       = 27;
    static const int MAGENTA    = 93;
    static const int BACKGROUND = 159;
    static const int WHITE      = 15;
    static const int GRAY       = 243;

    /**
     * Checks if terminal is big enough
     */
    void CheckTerminalSize();

    /**
     * Sets resources windows with correct aligns
     */
    void SetResourcesWindows();

    /**
     * Sets date window with correct aligns
     */
    inline void SetDateWindow() { m_DateWindow = newwin(1, COLS, 7, 0); };

    /**
     * Sets event window with correct aligns
     * @param eventWidth Event width on characters
     * @param eventHeight Event height on characters
     */
    void SetEventWindow(const int16_t & eventWidth,   const int16_t & eventHeight);

    /**
     * Sets map windows with correct aligns
     * @param mapCellWidth Map icon width on characters
     * @param mapCellHeight Map icon height on characters
     * @param mapSize Map size
     */
    void SetMapWindows(const int16_t & mapCellWidth, const int16_t & mapCellHeight, const Coord & mapSize);

    /**
     * Shows choices on cleared screen and waits for user input
     * @param choices Choices to be shown
     */
    void ShowChoices(const std::vector<const char *> & choices);

    /**
     * Shows message on cleared terminal
     * @param message Text to be displayed
     * @param error Flag determines if message is error or only info
     */
    void ShowMessage(const char * message, const bool & error);

    /**
     * Gets ASCII value of number from users input, except zero
     * @param maxValue Maximal value which user can enter
     * @return ASCII value of number from users input in range one to maxValue
     */
    uint8_t GetASCIINumber(const uint8_t & maxValue);

    /**
     * Waits until user press enter on keyboard
     */
    void WaitForEnter();

    /**
     * Loads two numbers to passed parameters from user input with maximal length of 9 characters and numbers separated
     * with one space
     * @param first First value to be set
     * @param second Second value to be set
     * @return true if user input was correct and numbers were parsed correctly, false otherwise
     */
    bool GetTwoNumbers(int16_t & first, int16_t & second);

public:

    /**
     * Constructor of Interface
     */
    Interface();

    /**
     * Destructor of Interface
     */
    ~Interface();

    /**
     * Initializes terminal colors and checks terminal size
     */
    void Initialize();

    /**
     * Sets all windows for initialized game with correct aligns
     * @param mapCellWidth Map icon width in characters
     * @param mapCellHeight Map icon height in characters
     * @param mapSize Size of game map
     * @param eventWidth Event width on characters
     * @param eventHeight Event height on characters
     */
    void SetAllWindows (const int16_t & mapCellWidth, const int16_t & mapCellHeight, const Coord & mapSize,
                        const int16_t & eventWidth,   const int16_t & eventHeight);

    /**
     * Checks if current game map fits to terminal
     * @param mapSize Size of game map
     * @return true if map can fit, false otherwise
     */
    inline bool CanFitMap (const Coord &mapSize) const
    { return (mapSize.m_X * 10 + 3) <= m_Cols && (mapSize.m_Y * 5 + 3) <= (m_Lines - 8); }

    /**
     * Refreshes all windows of interface after screen was cleared
     */
    void RefreshWindows();

    /**
     * Updates date window
     * @param day Day to be displayed
     * @param month Month to be displayed
     * @param year Year to be displayed
     */
    void UpdateDate(const int16_t & day, const int16_t & month, const int16_t & year);

    /**
     * Updates selected Resources window
     * @param value Actual generated value of selected resource to be displayed
     * @param limit Limit of selected resource to be displayed
     * @param fieldNumber Number of resources to be updated
     */
    void UpdateResource(const int16_t & value, const int16_t & limit, const int16_t & fieldNumber);

    /**
     * Updates selected map cell with ASCII art and color
     * @param coords Cell coordinates to be updated
     * @param image ASCII Art which fills selected cell
     * @param levelColor Color of cell background
     */
    void UpdateMapCell(const Coord & coords, const char * image, const int16_t & levelColor);

    /**
     * Clears map cell window (clears and sets background to white) with passed coordinates
     * @param coords Coordinates of map cell which should be cleared
     */
    void ClearMapCell(const Coord & coords);

    /**
     * Shows passed event visual on cleared terminal screen and waits to users check by pressing enter
     * @param visual ASCII Art visual to be shown
     */
    void ShowEvent(const char * visual);

    /**
     * Shows info message at centre of cleared terminal screen with blue background and waits until user presses enter
     * @param info Message to be show on terminal screen
     */
    void ShowInfo(const char * info);

    /**
     * Shows error message at centre of cleared terminal screen with red background and waits until user presses enter
     * @param error Message to be show on terminal screen
     */
    void ShowError(const char * message);

    /**
     * Checks if user pressed enter, non blocking read of input
     * @return true if user pressed enter, false otherwise
     */
    bool EnterPressed();

    /**
     * Checks if user somehow resized terminal, then prints error message
     * @return true, if terminal was resized, false otherwise
     */
    bool Resized();

    /**
     * Shows on cleared screen main menu choices and waits until user press valid input
     * @return E_MenuChoice enum value which user chose
     */
    E_MenuChoice ChooseInMainMenu();

    /**
     * Shows on cleared screen main menu choices and waits until user press valid input
     * @return E_MenuChoice enum value which user chose
     */
    E_Difficulty ChooseDifficulty();

    /**
     * Shows on cleared screen main menu choices and waits until user press valid input
     * @return E_MenuChoice enum value which user chose
     */
    E_ActionChoice ChooseAction();

    /**
     * Shows on cleared screen main menu choices and waits until user press valid input
     * @return E_MenuChoice enum value which user chose
     */
    E_BuildingChoice ChooseBuilding();

    /**
     * Shows on cleared screen main menu choices and waits until user press valid input
     * @return E_MenuChoice enum value which user chose
     */
    int16_t ChooseGameSlot();

    /**
     * Shows on cleared screen main menu choices and waits until user press valid input
     * @return E_MenuChoice enum value which user chose
     */
    Coord ChooseMapSize();

    /**
     * Shows on cleared screen main menu choices and waits until user press valid input
     * @return E_MenuChoice enum value which user chose
     */
    Coord ChooseCoords();
};