#include "Map.hpp"

Map::Map (const Coord & dimensions)
        : m_Dimensions(dimensions) {
    m_Data.clear();
}

Map::Map(FileReader &reader) {
    reader.Read(m_Dimensions.m_X, sizeof(m_Dimensions.m_X));
    reader.Read(m_Dimensions.m_Y, sizeof(m_Dimensions.m_Y));
}

void Map::Update (Resources & resources, Interface & interface) {
    /// iterate through every building in map and updates it's state
    for (auto & building : m_Data) {
        building.second->Update(resources);
        interface.UpdateMapCell(building.first, building.second->GetIconPointer() , building.second->GetLevel());
    }
}

bool Map::PositionFound (const Coord & coords ) const {
    auto itFound = m_Data.find(coords);
    /// if position isn't found, itFound is pointing to m_Data.end()
    return !(itFound == m_Data.end());
}

void Map::SaveDimensions (FileWriter & writer) const{
    writer.Write(m_Dimensions.m_X,sizeof(m_Dimensions.m_X));
    writer.Write(m_Dimensions.m_Y,sizeof(m_Dimensions.m_Y));

}

void Map::SaveContent (FileWriter & writer) const {
    /// iterates through every building, writes it's coordinates and pass writer to it for writing building's
    /// other parameters required for further correct load
    for (auto & building : m_Data) {
        writer.Write(building.first.m_X, sizeof(building.first.m_X));
        writer.Write(building.first.m_Y, sizeof(building.first.m_Y));
        building.second->Save(writer);
    }
}