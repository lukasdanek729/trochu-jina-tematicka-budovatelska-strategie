#pragma once

#include <vector>
#include <map>
#include <memory>

#include "Buildings/Building.hpp"
#include "Coord.hpp"

/**
 * Map class represents map of buildings, each on it's own unique coordinates fit in map's dimensions
 */
class Map {
private:

    Coord m_Dimensions;
    std::map<Coord, std::unique_ptr<Building>> m_Data;

public:

    /**
     * Map constructor with passed dimensions
     * @param dimensions Map dimensions which any building inserted cannot outreach
     */
    Map(const Coord &dimensions);

    /**
     * Map constructor with passed FileReader to load dimensions
     * @param reader File reader for reading map's dimensions
     */
    Map(FileReader &reader);

    /**
     * Map destructor
     */
    ~Map() = default;

    /**
     * Getter of map dimensions
     * @return map dimensions
     */
    inline const Coord GetDimensions() const { return m_Dimensions; }

    /**
     * Updates every building in map
     * @param resources Resources which updated building changes
     * @param interface Game interface which updates map cells depending on building state and it's coordinates
     */
    void Update(Resources &resources, Interface &interface);

    /**
     * Tries to insert building into map
     * @param coords Coordinates where building is inserted
     * @param building Clone of building
     * @return true if insert was successful, false otherwise
     */
    inline bool Insert(const Coord &coords, std::unique_ptr<Building> building) {
        return m_Data.emplace(std::make_pair(coords, std::move(building))).second;
    }

    /**
     * Tries to delete building from map
     * @param coords Coordinates of building to be deleted
     * @return true if delete was successful, false otherwise
     */
    inline bool Delete(const Coord &coords) { return m_Data.erase(coords) > 0; }

    /**
     * Makes building in passed coordinates available for further operations
     * @param coords Coordinates of building to be made available
     * @return reference to unique pointer of Building object
     */
    inline std::unique_ptr<Building> &MakeAvailable(const Coord &coords) { return m_Data[coords]; }

    /**
     * Checks if passed coordinates fit to map dimensions
     * @param coords Passed coordinates to be checked
     * @return true if passed coordinates fit to map dimensions, false otherwise
     */
    inline bool InBorders(const Coord &coords) const {
        return coords.m_X < m_Dimensions.m_X && coords.m_Y < m_Dimensions.m_Y;
    }

    /**
     * Checks if map contains building at passed coordinates
     * @param coords Passed coordinates
     * @return true if map contains building at passed coordinates, false otherwise
     */
    bool PositionFound(const Coord &coords) const;

    /**
     * Saves map dimensions to a file
     * @param writer File writer which cares about opening and writing correct bytes to a file
     */
    void SaveDimensions(FileWriter &writer) const;

    /**
     * Saves every building parameter in map to a file
     * @param writer File writer which cares about opening and writing correct bytes to a file
     */
    void SaveContent(FileWriter &writer) const;
};