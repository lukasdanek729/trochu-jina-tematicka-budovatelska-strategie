#include "Resources.hpp"


Resources::Resources(const E_Difficulty &difficulty) {
    /// sets starting values depending on chosen difficulty
    if (difficulty == E_Difficulty::ANTIQUITY) {
        SetValue(1500, 200, 200, 500, 200, 500);
        SetLimit(2000, 400, 400, 1500, 400, 2000);
    }
    if (difficulty == E_Difficulty::THESE_DAYS) {
        SetValue(1000, 150, 150, 500, 150, 500);
        SetLimit(1500, 300, 300, 1000, 300, 1500);
    }
    if (difficulty == E_Difficulty::MEDIEVAL) {
        SetValue(500, 150, 150, 150, 150, 250);
        SetLimit(750, 200, 200, 600, 300, 750);
    }
    SetIncrement(0, 0, 0, 0, 0, 0);
}

Resources::Resources(FileReader &reader) {
    /// loads every resource in order
    m_Money.Load(reader);
    m_IceCoffee.Load(reader);
    m_Cupcakes.Load(reader);
    m_Joy.Load(reader);
    m_Colors.Load(reader);
    m_Queers.Load(reader);
}

void Resources::SetValue(const uint32_t &money,
                         const uint32_t &iceCoffee,
                         const uint32_t &cupcakes,
                         const uint32_t &joy,
                         const uint32_t &colors,
                         const uint32_t &queers) {
    m_Money.m_Value       = money;
    m_IceCoffee.m_Value   = iceCoffee;
    m_Cupcakes.m_Value = cupcakes;
    m_Joy.m_Value         = joy;
    m_Colors.m_Value      = colors;
    m_Queers.m_Value      = queers;
}

void Resources::SetLimit(const uint32_t &money,
                         const uint32_t &iceCoffee,
                         const uint32_t &cupcakes,
                         const uint32_t &joy,
                         const uint32_t &colors,
                         const uint32_t &queers) {
    m_Money.m_Limit       = money;
    m_IceCoffee.m_Limit   = iceCoffee;
    m_Cupcakes.m_Limit = cupcakes;
    m_Joy.m_Limit         = joy;
    m_Colors.m_Limit      = colors;
    m_Queers.m_Limit      = queers;
}

void Resources::SetIncrement(const uint32_t &money,
                             const uint32_t &iceCoffee,
                             const uint32_t &cupcakes,
                             const uint32_t &joy,
                             const uint32_t &colors,
                             const uint32_t &queers) {
    m_Money.m_Increment       = money;
    m_IceCoffee.m_Increment   = iceCoffee;
    m_Cupcakes.m_Increment = cupcakes;
    m_Joy.m_Increment         = joy;
    m_Colors.m_Increment      = colors;
    m_Queers.m_Increment      = queers;
}

void Resources::Refresh(Interface &interface) {
    /// visualize resources update on terminal screen
    interface.UpdateResource(m_Money.m_Value, m_Money.m_Limit, 1);
    interface.UpdateResource(m_IceCoffee.m_Value, m_IceCoffee.m_Limit, 2);
    interface.UpdateResource(m_Cupcakes.m_Value, m_Cupcakes.m_Limit, 3);
    interface.UpdateResource(m_Joy.m_Value, m_Joy.m_Limit, 4);
    interface.UpdateResource(m_Colors.m_Value, m_Colors.m_Limit, 5);
    interface.UpdateResource(m_Queers.m_Value, m_Queers.m_Limit, 6);
}

bool Resources::Generate(Interface &interface) {
    /// check if joy value is above 6,25% (4x right shift => division by 16 => getting 6,25% of value) increment value
    /// of queers, then deduct same value from it, if not then returns false and game is over
    if (m_Joy.m_Value < ((m_Queers.m_Increment >> 4))) {
        interface.ShowError("Queers runned out of Joy, GAME OVER");
        return false;
    } else
        m_Joy.m_Value -= (m_Queers.m_Increment >> 4);

    m_Money.Update();
    m_IceCoffee.Update();
    m_Cupcakes.Update();
    m_Joy.Update();
    m_Colors.Update();
    m_Queers.Update();


    Refresh(interface);
    return true;
}

void Resources::UpdateValues(const std::initializer_list<uint32_t> &values, const std::uint8_t &mask) {
    auto valueIterator = values.begin();
    /// checks if operation is ADD
    bool operation = (mask & 0b10000000) == ADD;
    /// checks every bit in mask and depend on value updates corresponding resource value
    if (mask & MONEY) {
        operation ? m_Money.m_Value += *valueIterator : m_Money.m_Value -= *valueIterator;
        ++valueIterator;
    }
    if (mask & ICE_COFFEE) {
        operation ? m_IceCoffee.m_Value += *valueIterator : m_IceCoffee.m_Value -= *valueIterator;
        ++valueIterator;
    }
    if (mask & CUPCAKES) {
        operation ? m_Cupcakes.m_Value += *valueIterator : m_Cupcakes.m_Value -= *valueIterator;
        ++valueIterator;
    }
    if (mask & JOY) {
        operation ? m_Joy.m_Value += *valueIterator : m_Joy.m_Value -= *valueIterator;
        ++valueIterator;
    }
    if (mask & COLORS) {
        operation ? m_Colors.m_Value += *valueIterator : m_Colors.m_Value -= *valueIterator;
        ++valueIterator;
    }
    if (mask & QUEERS) {
        operation ? m_Queers.m_Value += *valueIterator : m_Queers.m_Value -= *valueIterator;
    }
}

void Resources::PercentageUpdateValues(const int16_t &percent, const std::uint8_t &mask) {
    /// checks if operation is ADD
    bool operation = (mask & ADD) == ADD;
    uint32_t calculatedValue;
    /// checks every bit in mask and depend on value updates corresponding resource value
    if (mask & MONEY) {
        calculatedValue = (m_Money.m_Value * percent) / 100;
        operation ? m_Money.m_Value += calculatedValue : m_Money.m_Value -= calculatedValue;
    }
    if (mask & ICE_COFFEE) {
        calculatedValue = (m_IceCoffee.m_Value * percent) / 100;
        operation ? m_IceCoffee.m_Value += calculatedValue : m_IceCoffee.m_Value -= calculatedValue;
    }
    if (mask & CUPCAKES) {
        calculatedValue = (m_Cupcakes.m_Value * percent) / 100;

        operation ? m_Cupcakes.m_Value += calculatedValue : m_Cupcakes.m_Value -= calculatedValue;
    }
    if (mask & JOY) {
        calculatedValue = (m_Joy.m_Value * percent) / 100;
        operation ? m_Joy.m_Value += calculatedValue : m_Joy.m_Value -= calculatedValue;
    }
    if (mask & COLORS) {
        calculatedValue = (m_Colors.m_Value * percent) / 100;
        operation ? m_Colors.m_Value += calculatedValue : m_Colors.m_Value -= calculatedValue;
    }
    if (mask & QUEERS) {
        calculatedValue = (m_Queers.m_Value * percent) / 100;
        operation ? m_Queers.m_Value += calculatedValue : m_Queers.m_Value -= calculatedValue;
    }
}

void Resources::UpdateLimit(const std::initializer_list<uint32_t> &values, const std::uint8_t &mask) {
    auto valueIterator = values.begin();
    /// checks if operation is ADD
    bool operation = (mask & ADD) == ADD;
    /// checks every bit in mask and depend on value updates corresponding resource limit
    if (mask & MONEY) {
        operation ? m_Money.m_Limit += *valueIterator : m_Money.m_Limit -= *valueIterator;
        ++valueIterator;
    }
    if (mask & ICE_COFFEE) {
        operation ? m_IceCoffee.m_Limit += *valueIterator : m_IceCoffee.m_Limit -= *valueIterator;
        ++valueIterator;
    }
    if (mask & CUPCAKES) {
        operation ? m_Cupcakes.m_Limit += *valueIterator : m_Cupcakes.m_Limit -= *valueIterator;
        ++valueIterator;
    }
    if (mask & JOY) {
        operation ? m_Joy.m_Limit += *valueIterator : m_Joy.m_Limit -= *valueIterator;
        ++valueIterator;
    }
    if (mask & COLORS) {
        operation ? m_Colors.m_Limit += *valueIterator : m_Colors.m_Limit -= *valueIterator;
        ++valueIterator;
    }
    if (mask & QUEERS) {
        operation ? m_Queers.m_Limit += *valueIterator : m_Queers.m_Limit -= *valueIterator;
    }
}

void Resources::UpdateIncrement(const std::initializer_list<uint32_t> &values, const std::uint8_t &mask) {
    auto valueIterator = values.begin();
    /// checks if operation is ADD
    bool operation = ((mask & ADD) == ADD);
    /// checks every bit in mask and depend on value updates corresponding resource increment
    if (mask & MONEY) {
        operation ? m_Money.m_Increment += *valueIterator : m_Money.m_Increment -= *valueIterator;
        ++valueIterator;
    }
    if (mask & ICE_COFFEE) {
        operation ? m_IceCoffee.m_Increment += *valueIterator : m_IceCoffee.m_Increment -= *valueIterator;
        ++valueIterator;
    }
    if (mask & CUPCAKES) {
        operation ? m_Cupcakes.m_Increment += *valueIterator : m_Cupcakes.m_Increment -= *valueIterator;
        ++valueIterator;
    }
    if (mask & JOY) {
        operation ? m_Joy.m_Increment += *valueIterator : m_Joy.m_Increment -= *valueIterator;
        ++valueIterator;
    }
    if (mask & COLORS) {
        operation ? m_Colors.m_Increment += *valueIterator : m_Colors.m_Increment -= *valueIterator;
        ++valueIterator;
    }
    if (mask & QUEERS) {
        operation ? m_Queers.m_Increment += *valueIterator : m_Queers.m_Increment -= *valueIterator;
    }

}

bool Resources::CheckValue(const std::initializer_list<uint32_t> &values, const std::uint8_t &mask) const {
    auto valueIterator = values.begin();
    /// checks every bit in mask and depend on value checks corresponding resource value
    if (mask & MONEY) {
        if (m_Money.m_Value < *valueIterator)
            return false;
        ++valueIterator;
    }
    if (mask & ICE_COFFEE) {
        if (m_IceCoffee.m_Value < *valueIterator)
            return false;
        ++valueIterator;
    }
    if (mask & CUPCAKES) {
        if (m_Cupcakes.m_Value < *valueIterator)
            return false;
        ++valueIterator;
    }
    if (mask & JOY) {
        if (m_Joy.m_Value < *valueIterator)
            return false;
        ++valueIterator;
    }
    if (mask & COLORS) {
        if (m_Colors.m_Value < *valueIterator)
            return false;
        ++valueIterator;
    }
    if (mask & QUEERS) {
        if (m_Queers.m_Value < *valueIterator)
            return false;
    }
    return true;
}

void Resources::Save(FileWriter &writer) const {
    m_Money.Save(writer);
    m_IceCoffee.Save(writer);
    m_Cupcakes.Save(writer);
    m_Joy.Save(writer);
    m_Colors.Save(writer);
    m_Queers.Save(writer);
}