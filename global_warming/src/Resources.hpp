#pragma once

#include "E_Difficulty.hpp"
#include "FileReader.hpp"
#include "FileWriter.hpp"
#include "Interface.hpp"

/**
 * Resources class represents every resource used in game and defines all operations with them
 */
class Resources {
private:

    /**
     * Represents every single resource and simple operations with it
     */
    struct Resource {
        uint32_t m_Value;
        uint32_t m_Limit;
        uint32_t m_Increment;

        /**
         * Updates resource value of it's increment and check overflowing over limit
         */
        void Update() {
            if (m_Value + m_Increment > m_Limit)
                m_Value = m_Limit;
            else
                m_Value += m_Increment;
        }

        /**
         * Loads every resource variable from a file
         * @param reader File reader for reading correct amounts of bytes and from correct file
         */
        void Load(FileReader &reader) {
            reader.Read(m_Value, sizeof(m_Value));
            reader.Read(m_Limit, sizeof(m_Limit));
            reader.Read(m_Increment, sizeof(m_Increment));
        }

        /**
         * Saves every resource variable to a file
         * @param writer File writer for writing resource variables to correct file
         */
        void Save(FileWriter &writer) const {
            writer.Write(m_Value, sizeof(m_Value));
            writer.Write(m_Limit, sizeof(m_Limit));
            writer.Write(m_Increment, sizeof(m_Increment));
        }
    };

    /**
     * Resources used in game
     */
    Resource m_Money;
    Resource m_IceCoffee;
    Resource m_Cupcakes;
    Resource m_Joy;
    Resource m_Colors;
    Resource m_Queers;

public:

    /**
     * Enum representing values which are used in mask for selected operations with selected resources
     */
    enum : uint8_t {
        ADD        = 0b10000000,
        DEDUCT     = 0b00000000,

        MONEY      = 0b00000001,
        JOY        = 0b00000010,
        COLORS     = 0b00000100,
        ICE_COFFEE = 0b00001000,
        CUPCAKES   = 0b00010000,
        QUEERS     = 0b01000000
    };

    /**
     * Constructor of Resources with passed difficulty on which amount of starting resources depends
     * @param difficulty Game difficulty which user chose
     */
    Resources(const E_Difficulty &difficulty);

    /**
     * Constructor of Resources with passed File reader for loading values from played game
     * @param reader File reader for reading correct amounts of bytes and from correct file
     */
    Resources(FileReader &reader);

    /**
     * Destructor of Resources
     */
    ~Resources() = default;

    /**
     * Set every Resource m_Value
     * @param money  Resources money value
     * @param iceCoffee Resources ice coffee value
     * @param cupcakes Resources cupcakes value
     * @param joy Resources joy value
     * @param colors Resources colors value
     * @param queers Resources queers value
     */
    void SetValue(const uint32_t &money,
                  const uint32_t &iceCoffee,
                  const uint32_t &cupcakes,
                  const uint32_t &joy,
                  const uint32_t &colors,
                  const uint32_t &queers);

    /**
     * Set every Resource m_Limit
     * @param money  Resources money limit
     * @param iceCoffee Resources ice coffee limit
     * @param cupcakes Resources cupcakes limit
     * @param joy Resources joy limit
     * @param colors Resources colors limit
     * @param queers Resources queers limit
     */
    void SetLimit(const uint32_t &money,
                  const uint32_t &iceCoffee,
                  const uint32_t &cupcakes,
                  const uint32_t &joy,
                  const uint32_t &colors,
                  const uint32_t &queers);

    /**
     * Set every Resource m_Value
     * @param money  Resources money increment
     * @param iceCoffee Resources ice coffee increment
     * @param cupcakes Resources cupcakes increment
     * @param joy Resources joy increment
     * @param colors Resources colors increment
     * @param queers Resources queers increment
     */
    void SetIncrement(const uint32_t &money,
                      const uint32_t &iceCoffee,
                      const uint32_t &cupcakes,
                      const uint32_t &joy,
                      const uint32_t &colors,
                      const uint32_t &queers);

    /**
     * Refresh counts of resources in game interface
     * @param interface Game interface where Resources update is visualized
     */
    void Refresh(Interface &interface);

    /**
     * Generates more resources, increases every resource value by it's increment
     * @param interface Game interface where Resources update is visualized
     * @return true if generating was successful, false otherwise
     */
    bool Generate(Interface &interface);

    /**
     * Check if Resources values are as big as passed ones
     * @param values List of values to compare Resources with
     * @param mask Mask representing Resources to be compared
     * @return true if Resources values are as big as passed ones, false otherwise
     */
    bool CheckValue(const std::initializer_list<uint32_t> &values, const std::uint8_t &mask) const;

    /**
     * Updates Resources values by passed percentage
     * @param percentage Percentage value used in Resources update
     * @param mask Mask representing Resources to be updated
     */
    void PercentageUpdateValues(const int16_t &percentage, const std::uint8_t &mask);

    /**
     * Updates Resources values by passed values
     * @param values List of values to be added or deducted from Resources values
     * @param mask Mask representing Resources to be updated
     */
    void UpdateValues(const std::initializer_list<uint32_t> &values, const std::uint8_t &mask);

    /**
     * Updates Resources limits by passed values
     * @param values List of values to be added or deducted from Resources limits
     * @param mask Mask representing Resources to be updated
     */
    void UpdateLimit(const std::initializer_list<uint32_t> &values, const std::uint8_t &mask);

    /**
     * Updates Resources increments by passed values
     * @param values List of values to be added or deducted from Resources increments
     * @param mask Mask representing Resources to be updated
     */
    void UpdateIncrement(const std::initializer_list<uint32_t> &values, const std::uint8_t &mask);

    /**
     * Saves every Resource to a file
     * @param writer File writer for writing every resource variables to correct file
     */
    void Save(FileWriter &writer) const;
};