#include "Time.hpp"

Time::Time(const E_Difficulty &difficulty)
        : m_Day(1),
          m_Month(1) {
    switch (difficulty) {
        /// sets correct epoch depending on difficulty
        case E_Difficulty::ANTIQUITY:
            m_Year = -690;
            break;
        case E_Difficulty::MEDIEVAL:
            m_Year = 1069;
            break;
        case E_Difficulty::THESE_DAYS:
            m_Day = 7;
            m_Month = 2;
            m_Year = 1999;
            break;
        default:
            m_Year = 0;
            break;
    }
}

Time::Time(FileReader &reader) {
    reader.Read(m_Day, sizeof(m_Day));
    reader.Read(m_Month, sizeof(m_Month));
    reader.Read(m_Year, sizeof(m_Year));
}

void Time::Update(Interface &interface) {
    /// checks over floating of count of day and months
    if (++m_Day > 30) {
        m_Day = 1;
        if (++m_Month > 12) {
            m_Month = 1;
            ++m_Year;
        }
    }
    Refresh(interface);
}

void Time::Save(FileWriter &writer) const {
    writer.Write(m_Day, sizeof(m_Day));
    writer.Write(m_Month, sizeof(m_Month));
    writer.Write(m_Year, sizeof(m_Year));
}
