#pragma once

#include "E_Difficulty.hpp"
#include "FileReader.hpp"
#include "FileWriter.hpp"
#include "Interface.hpp"

/**
 * Time class represents passed time in game
 */
class Time {
private:

    int16_t m_Day;
    int16_t m_Month;
    int16_t m_Year;

public:

    /**
     * Constructor of Time with passed difficulty on which starting date depends
     * @param difficulty Game difficulty which user chose
     */
    Time(const E_Difficulty &difficulty);

    /**
     * Constructor of Time with passed File reader for loading values from played game
     * @param reader File reader for reading correct amounts of bytes and from correct file
     */
    Time(FileReader &reader);

    /**
     * Destructor of Time
     */
    ~Time() = default;

    /**
     * Refresh Time in game interface
     * @param interface Game interface where Time refresh is visualized
     */
    inline void Refresh(Interface &interface) { interface.UpdateDate(m_Day, m_Month, m_Year); }

    /**
     * Updates Time jumping by one day and shows it in user interface
     * @param interface Game interface where Time update is visualized
     */
    void Update(Interface &interface);

    /**
     * Saves Actual date to a file
     * @param writer File writer for writing Time parameters to correct file
     */
    void Save(FileWriter &writer) const;

    /**
     * Checks if PRIDE date is set
     * @return true if PRIDE date is set, false otherwise
     */
    inline bool IsPride() const { return m_Day == 8 && m_Month == 8; }

};